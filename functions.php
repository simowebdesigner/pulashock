<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
	'lib/assets.php',    // Scripts and stylesheets
	'lib/extras.php',    // Custom functions
	'lib/setup.php',     // Theme setup
	'lib/titles.php',    // Page titles
	'lib/wrapper.php',   // Theme wrapper class
	'lib/customizer.php', // Google fonts
	'lib/demo-import.php', // One click demo import
	'plugin/tgm-setting.php', // Tgm plugin
	'acf-components/acf-setting.php', // Acf integration
];
foreach ($sage_includes as $file) {
	if (!$filepath = locate_template($file)) {
		trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
	}

	require_once $filepath;
}
unset($file, $filepath);

/*--------------------------*
Custom image resize
/*--------------------------*/
add_action( 'after_setup_theme', 'wpdocs_theme_setup' );
function wpdocs_theme_setup() {
	//add_image_size( 'portfolio-size', 800, 600, array( 'center', 'center' ) ); // (cropped)
	add_image_size( 'gallery-thumb', 400, 300, array( 'center', 'center' ) );
	add_image_size( 'news-thumb', 800, 500, array( 'center', 'center' ) );
}

/*--------------------------*
/* Excerpt Length
/*--------------------------*/

function stack_excerpt_length( $length ) {
	return 15;
}
add_filter( 'excerpt_length', 'stack_excerpt_length', 999 );


// Replaces the excerpt "more" text by a link
function new_excerpt_more($more) {
	global $post;
	return '<a class="moretag" href="'. get_permalink($post->ID) . '"> ...</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');

/*--------------------------*
/* Excerpt on pages
/*--------------------------*/

add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
	add_post_type_support( 'page', 'excerpt' );
}

/*******************
Custom css ACF
*******************/
function franky_customizer_css() {
	wp_enqueue_style('custom-style',  get_template_directory_uri() . '/custom-styles.css');
	require_once get_template_directory() . '/acf-components/custom-styles.php';
	$custom_inline_style = ($custom_inline_style) ? $custom_inline_style : $custom_inline_style = '';
	wp_add_inline_style( 'custom-style', $custom_inline_style );
}
add_action( 'wp_enqueue_scripts','franky_customizer_css',999);

/**
* enqueue scripts and styles 
*
*/
$google_key  = get_theme_mod('franky_map_google_key');
function my_acf_init() {

	acf_update_setting('google_api_key', 'AIzaSyCrMchFr_cBdFd8niZtN7rCONxO0bHgALs');
}

add_action('acf/init', 'my_acf_init');

/*--------------------------*
Google fonts
/*--------------------------*/

function wpb_add_google_fonts() {

	wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700,700i|Oswald:400,500,600,700|Playfair+Display&display=swap', false ); 
}
add_action( 'wp_enqueue_scripts', 'wpb_add_google_fonts' );

/*--------------------------*
Categories post type
/*--------------------------*/

function add_custom_types_to_tax( $query ) {
    if (is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {
        // Get all your post types
        $post_types = get_post_types();
        $query->set( 'post_type', $post_types );
        return $query;
    }
}
add_filter( 'pre_get_posts', 'add_custom_types_to_tax' );


/*--------------------------*
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
/*--------------------------*/
/*if ( ! isset( $content_width ) ) {
	$content_width = 1140;
}*/

/**
 * Infinite Scroll

add_theme_support( 'infinite-scroll', array(
	'type'           => 'scroll',
	'footer_widgets' => false,
	'container'      => 'content',
	'wrapper'        => true,
	'render'         => false,
	'posts_per_page' => false,
) ); */



/*--------------------------*
Function shortcode ajax load more
/*--------------------------*/

function franky_addLoadmore($arrayAddLoadmore) {
	$post_type = 	$arrayAddLoadmore['post_type'];
	$posts_per_page = $arrayAddLoadmore['posts_per_page'];
	$order = $arrayAddLoadmore['order'];
	$order_by = $arrayAddLoadmore['order_by'];
	$scroll = $arrayAddLoadmore['scroll'];
	$loading_label = $arrayAddLoadmore['loading_label'];
  $category= $arrayAddLoadmore['category'];
  $offset= $arrayAddLoadmore['offset'];
	$category__not_in = 	($arrayAddLoadmore['category__not_in']) ? $arrayAddLoadmore['category__not_in'] : '';
	$tag__not_in = 	($arrayAddLoadmore['tag__not_in']) ? $arrayAddLoadmore['tag__not_in'] : '';
	$progress_bar = 	($arrayAddLoadmore['progress_bar']) ? 'true': 'false';
	$progress_bar_color = 	($arrayAddLoadmore['progress_bar_color']) ? $arrayAddLoadmore['progress_bar_color'] : '000000';
	
	$string = 'do_shortcode';
	$shortCode = $string ("[ajax_load_more container_type=\"div\" css_classes=\"alm-reveal\" post_type=\"$post_type\" posts_per_page=\"$posts_per_page\" order=\"$order\" category=\"$category\" orderby=\"$order_by\" scroll=\"$scroll\" button_label=\"$loading_label\" category__not_in=\"$category__not_in\" offset=\"$offset\" tag__not_in=\"$tag__not_in\" progress_bar=\"$progress_bar\" filters=\"true\" progress_bar_color=\"$progress_bar_color\" transition=\"masonry\" masonry_selector=\".grid-item\" masonry_animation= \"slide-up\" ]");
	return $shortCode;
}

/*--------------------------*
Shortcode icons
/*--------------------------*/

function ionic_icons($atts) {
	extract(shortcode_atts(array(
		'class' => 'ion-ionic ',
		'size' => 'medium',
	), $atts));
	return '<i class="icons-custom-shortcode '. $class . ' ' . $size .'"></i>';
} 
add_shortcode('icon', 'ionic_icons');


/*--------------------------*
title acf flex
/*--------------------------*/

function my_acf_flexible_content_layout_title( $title, $field, $layout, $i ) {

	// remove layout title from text


	// load text sub field
	if( $text = strip_tags(get_sub_field('title_block')) ) {
		$title =  $title . ': <strong>' . $text . '</strong>';		
	} elseif( $text = strip_tags(get_sub_field('title')) ) {
		$title =  $title . ': <strong>' . $text . '</strong>';		
	}

	// return
	return $title;

}

// name
//add_filter('acf/fields/flexible_content/layout_title/name=flexible_content', 'my_acf_flexible_content_layout_title', 10, 4);

/*--------------------------*
post type
/*--------------------------*/

function cptui_register_my_cpts() {

	 //Post Type: Recensioni musicali.

	$labels = array(
		"name" => __( "Recensioni musicali", "sage" ),
		"singular_name" => __( "Recensione", "sage" ),
	);

	$args = array(
		"label" => __( "Recensioni musicali", "sage" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "recensioni", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail", "excerpt" ),
		"taxonomies" => array( "post_tag" ),
	);

	register_post_type( "recensioni", $args );

	 // Post Type: Biografie.
	 

	$labels = array(
		"name" => __( "Biografie", "sage" ),
		"singular_name" => __( "Biografia", "sage" ),
	);

	$args = array(
		"label" => __( "Biografie", "sage" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "biografie", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail", "excerpt", "revisions" ),
   "taxonomies" => array( "post_tag" ),
	);

	register_post_type( "biografie", $args );

// Post Type: Interviste.
	 

	$labels = array(
		"name" => __( "Interviste", "sage" ),
		"singular_name" => __( "Intervista", "sage" ),
	);

	$args = array(
		"label" => __( "Interviste", "sage" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "interviste", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail" ),
   "taxonomies" => array( "post_tag" ),
	);

	register_post_type( "interviste", $args );
}

add_action( 'init', 'cptui_register_my_cpts' );




