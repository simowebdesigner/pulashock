<?php

function ocdi_import_files() {
  return array(
    array(
      'import_file_name'           => 'App',
      'import_file_url'            => 'https://www.frankytheme.com/demo-import/app/demo-content.xml',
      'local_import_widget_file'     => trailingslashit( get_template_directory() ) . '/demo-import/app/widget.json',
      'local_import_customizer_file' => trailingslashit( get_template_directory() ) . '/demo-import/app/customizer.dat',
      'import_preview_image_url'   => 'https://www.frankytheme.com/wp-content/uploads/2017/05/app.jpg',
      'preview_url'                => 'https://www.frankytheme.com/app/',
    ),
    array(
      'import_file_name'           => 'Generic',
      'import_file_url'            => 'https://www.frankytheme.com/demo-import/generic/demo-content.xml',
      'local_import_widget_file'    => trailingslashit( get_template_directory() ) . '/demo-import/generic/widget.json',
      'local_import_customizer_file' => trailingslashit( get_template_directory() ) . '/demo-import/generic/customizer.dat',
      'import_preview_image_url'   => 'https://www.frankytheme.com/wp-content/uploads/2017/05/generic.jpg',
      'preview_url'                => 'https://www.frankytheme.com/generic/',
    ),
    array(
      'import_file_name'           => 'Corporate',
      'import_file_url'            => 'https://www.frankytheme.com/demo-import/corporate/demo-content.xml',
      'local_import_widget_file'    => trailingslashit( get_template_directory() ) . '/demo-import/corporate/widget.json',
      'local_import_customizer_file' => trailingslashit( get_template_directory() ) . '/demo-import/corporate/customizer.dat',
      'import_preview_image_url'   => 'https://www.frankytheme.com/wp-content/uploads/2017/05/corporate.jpg',
      'preview_url'                => 'https://www.frankytheme.com/corporate/',
    ),
    array(
      'import_file_name'           => 'One page',
      'import_file_url'            => 'https://www.frankytheme.com/demo-import/onepage/demo-content.xml',
      'local_import_widget_file'    => trailingslashit( get_template_directory() ) . '/demo-import/onepage/widget.json',
      'local_import_customizer_file' => trailingslashit( get_template_directory() ) . '/demo-import/onepage/customizer.dat',
      'import_preview_image_url'   => 'https://www.frankytheme.com/wp-content/uploads/2017/05/onepage.jpg',
      'preview_url'                => 'https://www.frankytheme.com/onepage/',
    ),
    array(
      'import_file_name'           => 'Freelance',
      'import_file_url'            => 'https://www.frankytheme.com/demo-import/freelance/demo-content.xml',
      'local_import_widget_file'    => trailingslashit( get_template_directory() ) . '/demo-import/freelance/widget.json',
      'local_import_customizer_file' => trailingslashit( get_template_directory() ) . '/demo-import/freelance/customizer.dat',
      'import_preview_image_url'   => 'https://www.frankytheme.com/wp-content/uploads/2017/05/FREELANCE.jpg',
      'preview_url'                => 'https://www.frankytheme.com/freelance/',
    ),
    array(
      'import_file_name'           => 'Studio',
      'import_file_url'            => 'https://www.frankytheme.com/demo-import/studio/demo-content.xml',
      'local_import_widget_file'    => trailingslashit( get_template_directory() ) . '/demo-import/studio/widget.json',
      'local_import_customizer_file' => trailingslashit( get_template_directory() ) . '/demo-import/studio/customizer.dat',
      'import_preview_image_url'   => 'https://www.frankytheme.com/wp-content/uploads/2017/05/studio.jpg',
      'preview_url'                => 'https://www.frankytheme.com/studio/',
    ),
  );
}
add_filter( 'pt-ocdi/import_files', 'ocdi_import_files' );

function ocdi_after_import_setup() {
   // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Top menu', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary_navigation' => $main_menu->term_id,
        )
    );
    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'ocdi_after_import_setup' );