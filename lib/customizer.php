<?php

namespace Roots\Sage\Customizer;

use Roots\Sage\Assets;

/**
 * Add postMessage support
 */
function customize_register($wp_customize) {
  $wp_customize->get_setting('blogname')->transport = 'postMessage';

  //add section logo
  $wp_customize->add_section( 'section_logo', array(
    'priority'       => 120,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '',
    'title'          => 'Logo',
    'description'    => '',
  ) );
  /********************
  // Logo 
  ********************/
  $wp_customize->add_setting( 'logo_image', array( 
    'sanitize_callback' => 'esc_url_raw',
  ) );
  $wp_customize->add_control(new \WP_Customize_Image_Control($wp_customize,'logo_image',array(
    'label' => __('Logo', 'franky'),
    'section' => 'section_logo',
    'settings' => 'logo_image',
    'transport' => 'postMessage'

  )));
  //logo mobile
  $wp_customize->add_setting( 'logo_image_mobile', array( 
    'sanitize_callback' => 'esc_url_raw',
  ) );
  $wp_customize->add_control(new \WP_Customize_Image_Control($wp_customize,'logo_image_mobile',array(
    'label' => __('Logo Mobile', 'franky'),
    'section' => 'section_logo',
    'settings' => 'logo_image_mobile',
    'transport' => 'postMessage',
    'sanitize_callback' => 'esc_url_raw',
  )));
  /********************
  // Colors
  ********************/
  //add section colors & style
  $wp_customize->add_section( 'section_color_style', array(
    'priority'       => 130,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '',
    'title'          => 'Colors & Style',
    'description'    => '',
  ) );
  // add color picker setting
  $wp_customize->add_setting( 'primary_color', array(
    'default' => '#FF756A',
    'sanitize_callback' => 'sanitize_hex_color',
  ) );
  // add color picker control
  $wp_customize->add_control( new \WP_Customize_Color_Control( $wp_customize, 'primary_color', array(
    'label' => __('Link Color (Primary Color)', 'franky'),
    'section' => 'section_color_style',
    'settings' => 'primary_color',
  )));

  // add picker setting hover
  $wp_customize->add_setting( 'link_color_hover', array(
    'default' => '#fc9f97',
    'sanitize_callback' => 'sanitize_hex_color',
  ) );
  // add color picker control
  $wp_customize->add_control( new \WP_Customize_Color_Control( $wp_customize, 'link_color_hover', array(
    'label' => __('Link Hover Color', 'franky'),
    'section' => 'section_color_style',
    'settings' => 'link_color_hover',
  )));

  // add picker setting text
  $wp_customize->add_setting( 'text_color', array(
    'default' => '#666',
    'sanitize_callback' => 'sanitize_hex_color',
  ) );
  // add color picker control
  $wp_customize->add_control( new \WP_Customize_Color_Control( $wp_customize, 'text_color', array(
    'label' => __('Text Color', 'franky'),
    'section' => 'section_color_style',
    'settings' => 'text_color',
  )));

  // add picker setting text
  $wp_customize->add_setting( 'title_color', array(
    'default' => '#333',
    'sanitize_callback' => 'sanitize_hex_color',
  ) );
  // add color picker control
  $wp_customize->add_control( new \WP_Customize_Color_Control( $wp_customize, 'title_color', array(
    'label' => __('Title Color', 'franky'),
    'section' => 'section_color_style',
    'settings' => 'title_color',
  )));
  /********************
  // Style custom
  ********************/
  // style button
  $wp_customize->add_setting('select_header', array(
    'default'        => 'value1',
    'capability'     => 'edit_theme_options',
    'type'           => 'option',
    'sanitize_callback' => 'esc_attr',
  ));

  $wp_customize->add_control('select_header', array(
    'label'      => __('Button style', 'franky'),
    'section'    => 'section_color_style',
    'settings'   => 'select_header',
    'type'       => 'radio',
    'choices'    => array(
      'rounded' => 'Rounded',
      'flat' => 'Flat',
    ),
  ));
  /********************
  // Header 
  ********************/
  $wp_customize->add_section( 'custom_header', array(
    'title' => __( 'Header','franky' ),
    'panel' => '', // Not typically needed.
    'priority' => 140,
    'capability' => 'edit_theme_options',
    'theme_supports' => '', // Rarely needed.,
  ));
  // check custom button
  $wp_customize->add_setting('check_button', array(
    'capability' => 'edit_theme_options',
    'type'       => 'option',
    'sanitize_callback' => 'esc_attr',
  ));

  $wp_customize->add_control('display_check_button', array(
    'settings' => 'check_button',
    'label'    => __('Show button custom','franky'),
    'section'  => 'custom_header',
    'type'     => 'checkbox',
  ));


  // text custom button
  $wp_customize->add_setting('text_button', array(
    'capability' => 'edit_theme_options',
    'type'       => 'option',
    'default' => 'Text',
    'sanitize_callback' => 'esc_attr',
  ));

  $wp_customize->add_control('text_button', array(
    'settings' => 'text_button',
    'label'    => __('Button text','franky'),
    'section'  => 'custom_header',
    'type'     => 'text',
  ));

  // url custom button
  $wp_customize->add_setting('url_button', array(
    'capability' => 'edit_theme_options',
    'type'       => 'option',
    'sanitize_callback' => 'esc_attr',
  ));

  $wp_customize->add_control('url_button', array(
    'settings' => 'url_button',
    'label'    => __('Button Link','franky'),
    'section'  => 'custom_header',
  ));
  // check custom search
  $wp_customize->add_setting('check_button_search', array(
    'capability' => 'edit_theme_options',
    'type'       => 'option',
    'sanitize_callback' => 'esc_attr',
  )); 
  $wp_customize->add_control('display_search_button', array(
    'settings' => 'check_button_search',
    'label'    => __('Show button search on menu','franky'),
    'section'  => 'custom_header',
    'type'     => 'checkbox',
  ));

  /********************
  // Footer 
  ********************/
  $wp_customize->add_section( 'custom_footer', array(
    'title' => __( 'Footer','franky' ),
    'panel' => '', // Not typically needed.
    'priority' => 150,
    'capability' => 'edit_theme_options',
    'theme_supports' => '', // Rarely needed.
  ));
  $wp_customize->add_setting('copy_footer', array(
    'capability' => 'edit_theme_options',
    'type'       => 'option',
    'sanitize_callback' => 'esc_attr',
  ));

  $wp_customize->add_control('copy_footer', array(
    'settings' => 'copy_footer',
    'label'    => __('Copyright','franky'),
    'section'  => 'custom_footer',
    'type'     => 'textarea',
  ));


  /********************
  // Post
  ********************/
  $wp_customize->add_section( 'blog_single', array(
    'title' => __( 'Single post','franky' ),
    'panel' => '', // Not typically needed.
    'priority' => 160,
    'capability' => 'edit_theme_options',
    'theme_supports' => '', // Rarely needed.,
  ));
  // check related post
  $wp_customize->add_setting('check_related', array(
    'capability' => 'edit_theme_options',
    'type'       => 'option',
    'sanitize_callback' => 'esc_attr',
  ));
  $wp_customize->add_control('display_check_related', array(
    'settings' => 'check_related',
    'label'    => __('Show related post by tags','franky'),
    'section'  => 'blog_single',
    'priority' => 2,
    'type'     => 'checkbox',
  ));
  // number related post
  $wp_customize->add_setting('number_related', array(
    'capability' => 'edit_theme_options',
    'type'       => 'option',
    'default' => '3',
    'sanitize_callback' => 'esc_attr',
  ));
  $wp_customize->add_control('number_post_related', array(
    'settings' => 'number_related',
    'label'    => __('Number related post','franky'),
    'section'  => 'blog_single',
    'priority' => 3,
    'type'     => 'number',
  ));

  // check image header
  $wp_customize->add_setting('check_image', array(
    'capability' => 'edit_theme_options',
    'type'       => 'option',
    'sanitize_callback' => 'esc_attr',
  ));
  $wp_customize->add_control('display_check_image', array(
    'settings' => 'check_image',
    'label'    => __('Show advanced image on page header','franky'),
    'section'  => 'blog_single',
    'priority' => 1,
    'type'     => 'checkbox',
  ));
  /********************
  // Portfolio
  ********************/
  $wp_customize->add_panel( 'portfolio', array(
    'title' => __( 'Portfolio and masonry options','franky' ),
    'priority' => 170,
  )); 

  // Portfolio single
  $wp_customize->add_section( 'portfolio_single', array(
    'title' => __( 'Single','franky' ),
    'panel' => 'portfolio', // Not typically needed.
    'description' => __( '<h2>Configure your portfolio page</h2><hr><h3 style="margin: 30px 0 0 24px;">Advanced image</h3>', 'franky' ),
    'capability' => 'edit_theme_options',
    'theme_supports' => '', // Rarely needed.,
  ));  
  // check image header
  $wp_customize->add_setting('check_image_portfolio', array(
    'capability' => 'edit_theme_options',
    'type'       => 'option',
    'sanitize_callback' => 'esc_attr',
  ));
  $wp_customize->add_control('display_check_image_portfolio', array(
    'settings' => 'check_image_portfolio',
    'label'    => __('Show advanced image on page header','franky'),
    'priority' => 1,
    'section'  => 'portfolio_single',
    'type'     => 'checkbox',
  ));
  // check content image header
  $wp_customize->add_setting('check_content_portfolio', array(
    'capability' => 'edit_theme_options',
    'type'       => 'option',
    'sanitize_callback' => 'esc_attr',
  ));
  $wp_customize->add_control('display_content_image_portfolio', array(
    'title' => __( 'Single portfolio','franky' ),
    'settings' => 'check_content_portfolio',
    'label'    => __('Show advanced image on content','franky'),
    'description' => __( '<h3 style="margin: 30px 0 0 0;">Meta info</h3>', 'franky' ),
    'priority' => 2,
    'section'  => 'portfolio_single',
    'type'     => 'checkbox',
  ));

  //show author
  $wp_customize->add_setting('show_meta_author_single', array(
    'default'        => true,
    'sanitize_callback' => 'esc_attr',
  ));
  $wp_customize->add_control('show_meta_author_single', array(
    'settings' => 'show_meta_author_single',
    'label'    => __('Show author','franky'),
    'description'    => __('Show author on single portfolio page','franky'),
    'priority' => 3,
    'section'  => 'portfolio_single',
    'type'     => 'checkbox',
  ));
  //show comments
  $wp_customize->add_setting('show_meta_comments_single', array(
    'default'        => true,
    'sanitize_callback' => 'esc_attr',
  ));
  $wp_customize->add_control('show_meta_comments_single', array(
    'settings' => 'show_meta_comments_single',
    'label'    => __('Show comments','franky'),
    'description'    => __('Show comments on single portfolio page','franky'),
    'priority' => 3,
    'section'  => 'portfolio_single',
    'type'     => 'checkbox',
  ));
  //show date
  $wp_customize->add_setting('show_meta_date_single', array(
    'default'        => true,
    'sanitize_callback' => 'esc_attr',
  ));
  $wp_customize->add_control('show_meta_date_single', array(
    'settings' => 'show_meta_date_single',
    'label'    => __('Show date','franky'),
    'description' => __( '<h3 style="margin: 20px 0 0 0;">Related portfolio</h3>', 'franky' ),
    'priority' => 4,
    'section'  => 'portfolio_single',
    'type'     => 'checkbox',
  ));
  // check related post
  $wp_customize->add_setting('check_related_portfolio', array(
    'capability' => 'edit_theme_options',
    'type'       => 'option',
    'sanitize_callback' => 'esc_attr',
  ));
  $wp_customize->add_control('display_portfolio_related', array(
    'settings' => 'check_related_portfolio',
    'label'    => __('Show related portfolio by tags','franky'),
    'priority' => 5,
    'section'  => 'portfolio_single',
    'type'     => 'checkbox',
  ));
  // number related post
  $wp_customize->add_setting('numbers_portfolio', array(
    'capability' => 'edit_theme_options',
    'type'       => 'option',
    'default' => '3',
    'sanitize_callback' => 'esc_attr',
  ));
  $wp_customize->add_control('number_portfolio_related', array(
    'settings' => 'numbers_portfolio',
    'label'    => __('Number related portfolio','franky'),
    'priority' => 6,
    'section'  => 'portfolio_single',
    'type'     => 'number',
  ));
  /********************
  // Portfolio list
  ********************/

  $wp_customize->add_section( 'meta_info', array(
    'title' => __( 'Lists','franky' ),
    'priority' => 1,
    'panel'    => 'portfolio',
  )); 
  //show author
  $wp_customize->add_setting('show_meta_author', array(
    'default'        => true,
    'capability'     => 'edit_theme_options',
  ));
  $wp_customize->add_control('show_meta_author', array(
    'settings' => 'show_meta_author',
    'label'    => __('Show author','franky'),
    'section'  => 'meta_info',
    'type'     => 'checkbox',
  ));
  //show comments
  $wp_customize->add_setting('show_meta_comments', array(
    'default'        => true,
    'capability'     => 'edit_theme_options',
  ));
  $wp_customize->add_control('show_meta_comments', array(
    'settings' => 'show_meta_comments',
    'label'    => __('Show comments','franky'),
    'section'  => 'meta_info',
    'type'     => 'checkbox',
  ));
  //show date
  $wp_customize->add_setting('show_meta_date', array(
    'default'        => true,
    'capability'     => 'edit_theme_options',
  ));
  $wp_customize->add_control('show_meta_date', array(
    'settings' => 'show_meta_date',
    'label'    => __('Show date','franky'),
    'section'  => 'meta_info',
    'type'     => 'checkbox',
  ));
  //show description
  $wp_customize->add_setting('show_meta_description', array(
    'default'        => false,
    'capability'     => 'edit_theme_options',
  ));
  $wp_customize->add_control('show_meta_description', array(
    'settings' => 'show_meta_description',
    'label'    => __('Show description','franky'),
    'description'    => __('<br><hr>','franky'),
    'section'  => 'meta_info',
    'type'     => 'checkbox',
  ));


  /********************
  // Map api
  ********************/

  $tmp_sectionname = "franky_map";
  $tmp_default = "";
  $wp_customize->add_section( $tmp_sectionname . '_section', array(
    'title' => esc_html__( 'Google map api key', 'franky' ),
    'priority' => 190,
  ) );
  $tmp_settingname = $tmp_sectionname . '_google_key';

  $wp_customize->add_setting( $tmp_settingname, array(
    'default' => '',
    'sanitize_callback' => 'esc_attr'
  ) );

  $wp_customize->add_control( $tmp_settingname . '_control', array(
    'label' => esc_html__( 'Insert you google map api key', 'franky' ),
    'section' => $tmp_sectionname . "_section",
    'settings' => $tmp_settingname,
    'description' => esc_html__( '(see https://developers.google.com/maps/documentation/javascript/get-api-key#key )', 'franky' ),
    'type' => 'text',
  ) );

  /********************
  // Social 
  ********************/

  // add new section social
  $wp_customize->add_section( 'franky_social_options', array(
    'title' => __( 'Social', 'franky' ),
    'priority' => 180,
  ) );
  $wp_customize->add_setting( 'facebook_text', array(
    'default' => '',
    'sanitize_callback' => 'sanitize_text_field',
    'priority' => 2
  ) );

  $wp_customize->add_control( new \WP_Customize_Control( $wp_customize, 'facebook_text', array(
    'label'   => __( 'Facebook username', 'franky' ),
    'section' => 'franky_social_options',
    'settings'   => 'facebook_text',

  ) ) );

  $wp_customize->add_setting( 'twitter_text', array(
    'default' => '',
    'sanitize_callback' => 'sanitize_text_field',
    'priority' => 1
  ) );

  $wp_customize->add_control( new \WP_Customize_Control( $wp_customize, 'twitter_text', array(
    'label'   => __( 'Twitter username', 'franky' ),
    'section' => 'franky_social_options',
    'settings'   => 'twitter_text',

  ) ) );

  $wp_customize->add_setting( 'pinterest_text', array(
    'default' => '',
    'sanitize_callback' => 'esc_url_raw',
    'priority' => 3
  ) );

  $wp_customize->add_control( new \WP_Customize_Control( $wp_customize, 'pinterest_text', array(
    'label'   => __( 'Pinterest url', 'franky' ),
    'section' => 'franky_social_options',
    'settings'   => 'pinterest_text',

  ) ) );

  $wp_customize->add_setting( 'google1_text', array(
    'default' => '',
    'sanitize_callback' => 'esc_url_raw',
    'priority' => 4
  ) );


  $wp_customize->add_setting( 'dribbble_text', array(
    'default' => '',
    'sanitize_callback' => 'esc_url_raw',
    'priority' => 5
  ) );

  $wp_customize->add_control( new \WP_Customize_Control( $wp_customize, 'dribbble_text', array(
    'label'   => __( 'Dribbble url', 'franky' ),
    'section' => 'franky_social_options',
    'settings'   => 'dribbble_text',

  ) ) );

  $wp_customize->add_setting( 'flickr_text', array(
    'default' => '',
    'sanitize_callback' => 'esc_url_raw',
    'priority' => 6
  ) );

  $wp_customize->add_control( new \WP_Customize_Control( $wp_customize, 'flickr_text', array(
    'label'   => __( 'Flickr url', 'franky' ),
    'section' => 'franky_social_options',
    'settings'   => 'flickr_text',

  ) ) );

  $wp_customize->add_setting( 'youtube_text', array(
    'default' => '',
    'sanitize_callback' => 'esc_url_raw',
    'priority' => 7
  ) );

  $wp_customize->add_control( new \WP_Customize_Control( $wp_customize, 'youtube_text', array(
    'label'   => __( 'YouTube url', 'franky' ),
    'section' => 'franky_social_options',
    'settings'   => 'youtube_text',

  ) ) );

  $wp_customize->add_setting( 'instagram_text', array(
    'default' => '',
    'sanitize_callback' => 'esc_url_raw',
    'priority' => 8
  ) );

  $wp_customize->add_control( new \WP_Customize_Control( $wp_customize, 'instagram_text', array(
    'label'   => __( 'Instagram url', 'franky' ),
    'section' => 'franky_social_options',
    'settings'   => 'instagram_text',

  ) ) );

  $wp_customize->add_setting( 'vimeo_text', array(
    'default' => '',
    'sanitize_callback' => 'esc_url_raw',
    'priority' => 9
  ) );

  $wp_customize->add_control( new \WP_Customize_Control( $wp_customize, 'vimeo_text', array(
    'label'   => __( 'Vimeo url', 'franky' ),
    'section' => 'franky_social_options',
    'settings'   => 'vimeo_text',

  ) ) );

  $wp_customize->add_setting( 'behance_text', array(
    'default' => '',
    'sanitize_callback' => 'esc_url_raw',
    'priority' => 10
  ) );

  $wp_customize->add_control( new \WP_Customize_Control( $wp_customize, 'behance_text', array(
    'label'   => __( 'Behance url', 'franky' ),
    'section' => 'franky_social_options',
    'settings'   => 'behance_text',

  ) ) );

  $wp_customize->add_setting( 'linkedin_text', array(
    'default' => '',
    'sanitize_callback' => 'esc_url_raw',
    'priority' => 1
  ) );

  $wp_customize->add_control( new \WP_Customize_Control( $wp_customize, 'linkedin_text', array(
    'label'   => __( 'LinkedIn url', 'franky' ),
    'section' => 'franky_social_options',
    'settings'   => 'linkedin_text',

  ) ) );

  $wp_customize->add_setting( 'gplus_text', array(
    'default' => '',
    'sanitize_callback' => 'esc_url_raw',
    'priority' => 1
  ) );

  $wp_customize->add_control( new \WP_Customize_Control( $wp_customize, 'gplus_text', array(
    'label'   => __( 'Google+ url', 'franky' ),
    'section' => 'franky_social_options',
    'settings'   => 'gplus_text',

  )));

}
add_action('customize_register', __NAMESPACE__ . '\\customize_register');

/**
 * Customizer JS
 */
function customize_preview_js() {
  wp_enqueue_script('sage/customizer', Assets\asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
}
add_action('customize_preview_init', __NAMESPACE__ . '\\customize_preview_js');





