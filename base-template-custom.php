<?php session_start(); ?>
<?php
use Roots\Sage\Setup;
use Roots\Sage\Wrapper;
?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
  <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9Z7Q7S"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <div class="no-animsition">
      <?php //include humburger-menu
      get_template_part('templates/humburger-menu');
      ?>
      <?php 
      do_action('get_header');
      get_template_part('templates/header');
      $classHeader = '';
      if( class_exists('acf') ) {
        $select_page_header = get_field('select_page_header');
        if ($select_page_header == 'static_image' || $select_page_header == 'none' ) { 
          get_template_part('templates/page-header-page');
        }
        elseif ($select_page_header == 'slider') {
          $post_object = get_field('select_slide');
          if( $post_object ): 
          // override $post
          $post = $post_object;
          setup_postdata( $post );
          get_template_part('acf-components/slide');
          endif;
        } else {
          $classHeader = 'no-image-header';
        }
        wp_reset_postdata();
      ?>
      <div class="main-wrapper container">
        <div class="row">
          <main class="main col-md-9"> 
            <?php // include feature-icons
        get_template_part('acf-components/flexible-content');
      } else {
        include Wrapper\template_path(); 
      } ?>
          </main>
          <?php if (Setup\display_sidebar()) : ?>
          <div class="col-md-3">
            <aside class="sidebar">
              <?php include Wrapper\sidebar_path(); ?>
            </aside><!-- /.sidebar -->
          </div>
          <?php endif; ?>
        </div> 
      </div>
      <?php
      comments_template('/templates/comments.php');
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
      get_template_part('acf-components/custom-scripts');
      ?>

    </div>
  </body>
</html>