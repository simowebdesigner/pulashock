<?php 
session_start();
$style_blog = get_field('style_blog');
$data_taxonomy = ($_SESSION['data_taxonomy']) ? $_SESSION['data_taxonomy'] : 'category';
$terms = get_the_terms( $post->ID, $data_taxonomy );
$overlay_color = ($_SESSION['overlay_color']) ? $_SESSION['overlay_color'] : '';
$overlay_opacity = (isset($_SESSION['overlay_opacity'])) ? $_SESSION['overlay_opacity'] : '0.3';
$opacity = 'opacity: '. $overlay_opacity .';';
$overlay_hover_opacity = (isset($_SESSION['overlay_hover_opacity'])) ? $_SESSION['overlay_hover_opacity'] : '0.3';
$opacity_hover = 'opacity: '. $overlay_hover_opacity .';';
$image_featured = ($_SESSION['image_featured']) ? $_SESSION['image_featured'] : 'portfolio-size';
$show_meta_description = get_theme_mod('show_meta_description');
if ( $terms && ! is_wp_error( $terms ) ) : 
$links = array();
foreach ( $terms as $term ) 
{
	$links[] = $term->name;
}
$links = str_replace(' ', '-', $links); 
$tax = join( " ", $links );     
else :  
$tax = '';  
endif;
?>

<div class="grid-item <?php echo strtolower($tax); ?>">
	<?php 
	$post_type = get_post_type();
	$post_type_label = get_post_type_object( get_post_type($post) );
	$primary_color = get_field('primary_color');
	if ($overlay_color) {
		$primary_color = ($overlay_color) ?'background-color:'. $overlay_color .';' : 'background-color:#000;';
	}	else {
		$primary_color = ($primary_color) ?'background-color:'. $primary_color .';' : 'background-color:#000;';
	}
	?>
		<div class="box-img">
			<a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>">
				<div class="overlay" style="<?php echo esc_html($primary_color . $opacity); ?>"></div>
				<?php if ( has_post_thumbnail() ) { ?>
				<?php the_post_thumbnail($image_featured, array('class' => 'img-responsive')); ?>
				<?php } else { ?>
				<img src="<?php echo get_template_directory_uri(); ?>/dist/images/placeholder.jpg" alt="placeholder">
				<?php } ?>
			</a>
		</div>
		<div class="caption-description">
			<?php if ($post_type != 'post') { ?>
			<div class="italic">
				<a href="<?php echo get_post_type_archive_link( $post_type ); ?>"><?php echo $post_type_label->label ; ?></a>
			</div>
			<?php } ?>
			<a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>">
				<h4 class="caption-title"><?php the_title(); ?></h4>
			</a>
			<?php if ($show_meta_description) { ?>
			<div class="caption-text">
				<?php the_excerpt(); ?>
			</div>
			<?php } ?>
			<?php get_template_part('templates/meta-portfolio-list'); ?>
		</div>
</div>