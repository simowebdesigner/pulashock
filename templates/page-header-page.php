<?php use Roots\Sage\Titles; ?>
<?php if( class_exists('acf') ) : ?>
<?php //check image header post
$select_page_header = get_field('select_page_header');
$show_image = get_field('static_image');
$show_image_url = $show_image['url'];
$title_image = get_field('title_image');
$color_title = get_field('color_title');
$color_title = ($color_title)? 'color:'. $color_title . ';' : 'color:#fff;';
$caption_description = get_field('text_image');
$color_text = get_field('color_text');
$color_text = ($color_text)? 'color:'. $color_text . ';' : 'color:#fff;';
$heightSlide = get_field('max_height');
$units = get_field('units');
$overlay = get_field('overlay');
$opacity = get_field('opacity');
$opacity = ($opacity) ? 'opacity:' . $opacity . ';' : 'opacity:0.4;';
$overlay = ($overlay) ? 'background-color:' . $overlay . ';' : 'background-color:#000;';
$maxHeight = ($heightSlide)? 'max-height:'. $heightSlide . $units . ';' : 'max-height:100vh;'; 

?>
<?php if ($show_image && $select_page_header =='static_image') : ?>
<div id="content-slide">
  <div class="slide-header">
    <div class="swiper-container swiper-container-slide" <?php echo 'style="' . esc_html($maxHeight) . '"'; ?>>
      <div class="swiper-wrapper">
        <div class="swiper-slide swiper-slide-active slide-parallax text-center" <?php if ($show_image) { echo 'style="background-image:url(' . esc_url($show_image_url) . ')"'; } ?>>
          <div class="overlay" style="<?php echo esc_html($overlay); echo esc_html($opacity); ?>"></div>
          <div class="caption-tex">
            <?php if ($title_image) { ?>
            <div data-enllax-ratio="-0.1" data-enllax-type="foreground">
              <h1><span class="title-slide fadeAnimation" style="<?php echo esc_html($color_title); ?>"><?php echo esc_html($title_image); ?></span></h1>
            </div>
            <?php } ?>
            <?php if( $caption_description): ?>
            <div class="text-slide fadeAnimation" style="<?php echo esc_html($color_text); ?>">
              <?php the_field('text_image'); ?>	
            </div>
            <?php endif; ?>
            <?php if (get_field('breadcrumb')) { ?>
            <div class="page-block-heade mb-0">
              <div class="breadcrumbs text-center mb-0" typeof="BreadcrumbList" vocab="http://schema.org/">
               <small>
                <?php if(function_exists('bcn_display')) {
  bcn_display(); } ?></small>
              </div>
            </div>
            <?php } ?>
          </div>
          <div class="preloader"></div>
        </div>
      </div>
    </div>
  </div><!--end Slide header-->
</div><!--end content-slide-->
<?php else : ?>
<script>
  var b = document.getElementsByTagName("BODY")[0];
  b.classList.add("no-header-image");
</script>
<?php endif; ?>

<?php endif; ?>