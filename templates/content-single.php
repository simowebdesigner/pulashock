<?php

use Roots\Sage\Titles;  ?>
<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
    <div class="content-post">
      <?php
      $title = Titles\title(); ?>
      <h1 class="title-header mb-10 mt-30"><?php echo $title; ?></h1>
      <hr class="mb-10 mt-20">
      <div class="mb-40 row">
        <div class="col-sm-6">
          <?php get_template_part('templates/entry-meta'); ?>
        </div>
        <div class="col-sm-6">
          <?php if (function_exists('sharing_display')) {
            echo sharing_display();
          } ?>
        </div>
      </div>
      <div class="row">
        <?php
        //vars
        $image = get_field('cover');
        $author = get_field('author');
        $description = get_field('description');
        $publication = get_field('publication');
        $link1 = get_field('button_1');
        $link2 = get_field('button_2');
        if (!empty($image)) : ?>
          <div class="col-sm-4">
            <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
          </div>
        <?php else : ?>
          <?php
          if (has_post_thumbnail()) {
            echo '<div class="mb-40">';
            the_post_thumbnail();
            echo '</div>';
          } else {
            echo '<div class="mb-40">';
            the_post_thumbnail('medium', array('class' => 'alignleft'));
            echo '</div>';
          }
          ?>
        <?php endif; ?>




        <div class="col-sm-8">
          <?php
          $date_string = get_field('publication');
          $date = new \DateTime($date_string);
          ?>
          <?php if ($author || $publication) : ?>
            <p><?php echo __('<strong>Autore</strong>: ', 'start') . $author . __('<br> <strong>Anno di pubblicazione</strong>: ', 'start') . $date->format('Y'); ?></p>
          <?php endif; ?>
          <?php if ($description) : ?>
            <p><?php echo $description;  ?></p>
          <?php endif; ?>
          <div>
            <?php
            //primo pulsante
            if ($link1) :
              $link_url = $link1['url'];
              $link_title = $link1['title'];
              $link_target = $link1['target'] ? $link1['target'] : '_self';
            ?>
              <a class="btn btn-readmore btn-amazon" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
            <?php endif; ?>
            <?php
            //secondo pulsante
            if ($link2) :
              $link_url = $link2['url'];
              $link_title = $link2['title'];
              $link_target = $link2['target'] ? $link2['target'] : '_self';
            ?>
              <a class="btn btn-readmore btn-amazon" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
            <?php endif; ?>
          </div>
        </div>
      </div>

      <?php the_content(); ?>

      <?php $images = get_field('gallery');
      if ($images) : ?>
        <figure>
          <div class="swiper-container gallery-carousel mt-40">
            <div class="swiper-wrapper">
              <?php foreach ($images as $image) : ?>
                <div class="swiper-slide">
                  <a class="lightbox-image" data-lightbox="example-set" href="<?php echo esc_html($image['url']); ?>">
                    <div class="overlay"></div>
                    <img src="<?php echo esc_html($image['sizes']['gallery-thumb']); ?>" alt="<?php echo esc_html($image['alt']); ?>" />
                  </a>
                  <p><?php echo esc_html($image['caption']); ?></p>
                </div>
              <?php endforeach; ?>
            </div>
            <!-- Add Pagination 
<div class="swiper-pagination"></div>-->
            <!-- Add Arrows -->
            <div class="swiper-button-next swiper-button-white"></div>
            <div class="swiper-button-prev swiper-button-white"></div>
          </div>
        </figure>
      <?php endif; ?>
      <hr>

      <div class="entry-tags">
        <?php the_tags('Tags: ', ''); ?>
      </div>

      <?php if (!is_attachment()) : ?>
        <nav class="post-nav">
          <ul class="pagination mb-0">
            <li class="previous"><?php previous_post_link('%link', '&larr; %title'); ?></li>
            <li class="next"><?php next_post_link('%link', '%title &rarr;'); ?></li>
          </ul>
        </nav>
      <?php endif; ?>
    </div>

  </article>
<?php endwhile; ?>