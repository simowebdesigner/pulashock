<form method="get" action="<?php echo esc_html(home_url( '/' )); ?>">
	<div class="search-container">
		<input type="search" title="Search for:" name="s" value="" placeholder="<?php echo __('Search', 'franky') ?>" class="search-field">
	</div>
</form>