<article <?php post_class(); ?>>
	<div class="content-post" data-vp-add-class="fadeIn animated" >
	<?php $check_content_portfolio = get_option('check_content_portfolio'); ?>
	<?php if ($check_content_portfolio) : ?>
		<?php if ( has_post_thumbnail()): ?> 
		<figure>
				<?php echo get_the_post_thumbnail($post->ID, 'large', array('class' => 'img-responsive')); ?>
		</figure>
		<?php endif; ?>
		<?php endif; ?>
		<div class="box-text">
			<header>
				<h1 class="entry-title"><?php the_title(); ?></h1>
				<?php get_template_part('templates/entry-meta'); ?>
			</header>
			<div class="content-text mt-40">
				<?php echo $post->post_content; ?>
			</div>
		</div>
	</div>
</article>