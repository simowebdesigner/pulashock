<article <?php post_class(); ?>>
	<div class="content-post" data-vp-add-class="fadeIn animated" >
		<?php $check_content_portfolio = get_option('check_content_portfolio'); ?>
		<?php if ($check_content_portfolio) : ?>
		<?php if ( has_post_thumbnail()): ?> 
		<figure>
			<?php echo get_the_post_thumbnail($post->ID, 'large', array('class' => 'img-responsive')); ?>
		</figure>
		<?php endif; ?>
		<?php endif; ?>
		<div class="box-text">
			<header>
				<h1 class="entry-title"><?php the_title(); ?></h1>
				<?php get_template_part('templates/entry-meta'); ?>
			</header>
			<div class="content-text mt-40">
				<?php echo $post->post_content; ?>
				<?php $contact = get_field('contact');	
				if( have_rows('contact') ): 

				while( have_rows('contact') ): the_row(); 
				// vars
				$email = get_sub_field('email');
				$phone = get_sub_field('phone');
				$facebook = get_sub_field('facebook');
				$twitter = get_sub_field('twitter');
				$pinterest = get_sub_field('pinterest');
				$dribbble = get_sub_field('dribbble');
				$flickr = get_sub_field('flickr');	
				$youtube = get_sub_field('youtube');
				$instagram = get_sub_field('instagram');
				$vimeo = get_sub_field('vimeo');
				$behance = get_sub_field('behance');
				$github = get_sub_field('github');
				?>
				<div id="contact-team">
					<?php if(!empty($email)) : ?>
					<a href="mailto:<?php echo esc_html($email);?>" target="_blank" class="contact-icon"><i class="ion-ios-email"></i></a>
					<?php endif; ?>
					<?php if(!empty($phone))  : ?>
					<a href="tel:<?php echo esc_html($phone);?>" target="_blank" class="contact-icon"><i class="ion-ios-telephone"></i></a>
					<?php endif; ?>
					<?php if(!empty($facebook))  : ?>
					<a href="<?php echo esc_url($facebook);?>" target="_blank" class="contact-icon"><i class="ion-social-facebook"></i></a>
					<?php endif; ?>
					<?php if(!empty($twitter))  : ?>
					<a href="<?php echo esc_url($twitter);?>" target="_blank" class="contact-icon"><i class="ion-social-twitter"></i></a>
					<?php endif; ?>
					<?php if(!empty($pinterest))  : ?>
					<a href="<?php echo esc_url($pinterest);?>" target="_blank" class="contact-icon"><i class="ion-social-pinterest"></i></a>
					<?php endif; ?>
					<?php if(!empty($dribbble))  : ?>
					<a href="<?php echo esc_url($dribbble);?>" target="_blank" class="contact-icon"><i class="ion-social-dribbble"></i></a>
					<?php endif; ?>
					<?php if(!empty($flickr))  : ?>
					<a href="<?php echo esc_url($flickr);?>" target="_blank" class="contact-icon"><i class="ion-social-flickr"></i></a>
					<?php endif; ?>
					<?php if(!empty($youtube))  : ?>
					<a href="<?php echo esc_url($youtube);?>" target="_blank" class="contact-icon"><i class="ion-social-youtube"></i></a>
					<?php endif; ?>
					<?php if(!empty($instagram))  : ?>
					<a href="<?php echo esc_url($instagram);?>" target="_blank" class="contact-icon"><i class="ion-social-instagram"></i></a>
					<?php endif; ?>
					<?php if(!empty($vimeo))  : ?>
					<a href="<?php echo esc_url($vimeo);?>" target="_blank" class="contact-icon"><i class="ion-social-vimeo"></i></a>
					<?php endif; ?>
					<?php if(!empty($behance))  : ?>
					<a href="<?php echo esc_url($behance);?>" target="_blank" class="contact-icon"><i class="ion-social-behance"></i></a>
					<?php endif; ?>
					<?php if(!empty($github))  : ?>
					<a href="<?php echo esc_url($github);?>" target="_blank" class="contact-icon"><i class="ion-social-github"></i></a>
					<?php endif; ?>
				</div>
				<?php endwhile; ?>

				<?php endif; ?>
			</div>


		</div>
	</div>
</article>