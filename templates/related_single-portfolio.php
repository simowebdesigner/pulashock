<div class="grid-item fade-scroll style-default" data-vp-add-class="fadeIn animated">
	<div class="box-img">
				<a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>">
				<div class="overlay"></div>
				<?php if ( has_post_thumbnail() ) { ?>
				<?php the_post_thumbnail('portfolio-size', array('class' => 'img-responsive')); ?>
				<?php } else { ?>
				<img src="<?php echo get_template_directory_uri(); ?>/dist/images/placeholder.jpg" alt="placeholder">
				<?php } ?>
		</a>
		</div>
		<div class="caption-description">
			<?php if ($post_type) { ?>
			<div class="italic">
				<?php the_category(', '); ?>
			</div>
			<?php } ?>
			<a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>">
				<h4 class="caption-title"><?php the_title(); ?></h4>
			</a>
			<?php get_template_part('templates/meta-portfolio-list'); ?>
		</div>
</div>