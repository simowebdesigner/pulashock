<?php 
$show_meta_author = get_theme_mod( 'show_meta_author');
$show_meta_comments = get_theme_mod('show_meta_comments');
$show_meta_date = get_theme_mod('show_meta_date');

?>
<?php if ($show_meta_comments || $show_meta_author || $show_meta_date) { ?>
<div class="caption-info">
	<?php if ($show_meta_comments) { ?>
	<div class="info-comments">
		<i class="ion-ios-chatboxes-outline"></i> <?php printf( _nx( '', '%1$s', get_comments_number(), 'comments title', 'textdomain' ), number_format_i18n( get_comments_number() ) ); ?>
	</div>
	<?php } ?>
	<?php if ($show_meta_date) { ?>
	<div class="info-data">
		<!--date-->
		<?php $date = get_the_date('d.m.y'); ?>
		<?php $time = get_post_time('c', true); ?>
		<i class="ion-ios-calendar-outline"></i> <time class="updated" datetime="<?php echo esc_html($time); ?>"><span><?php echo $date; ?></span></time>
	</div>
	<?php } ?>
	<?php if ($show_meta_author) { ?>
	<div class="info-author">
		<!--author-->
		<?php $author = get_the_author(); ?>
		<?php $url = admin_url(); ?>
		<i class="ion-ios-person-outline"></i>
		<span><?php _e('by', 'franky'); ?></span> <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" rel="author" class="fn"><?php the_author_meta( 'display_name' ); ?></a>
	</div>
	<?php } ?>
</div>
	<?php } ?>