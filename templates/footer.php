<footer id="footer" class="content-info">
	<section class="footer-top">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<?php dynamic_sidebar('footer1'); ?>
				</div>
				<div class="col-sm-3">
					<?php dynamic_sidebar('footer2'); ?>
				</div>
				<div class="col-sm-3">
					<?php dynamic_sidebar('footer3'); ?>
				</div>
				<div class="col-sm-3">
					<?php dynamic_sidebar('footer4'); ?>
				</div>
			</div>
		</div>
	</section>
	<section class="footer-bottom ">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 text-left">
					<?php $copyright = get_option('copy_footer'); ?>
					<?php if ($copyright) : ?>
					<div class="mt-20"><?php echo esc_html($copyright); ?></div>
					<?php endif; ?>
				</div>
				<div class="col-sm-6 text-right">
					<ul class="social-icons">
					<?php 
						$twitter  = get_theme_mod('twitter_text');  
						$facebook  = get_theme_mod('facebook_text');  
						$dribbble  = get_theme_mod('dribbble_text');  
						$pinterest  = get_theme_mod('pinterest_text');  
						$vimeo  = get_theme_mod('vimeo_text');  
						$flickr  = get_theme_mod('flickr_text');  
						$youtube  = get_theme_mod('youtube_text');  
						$behance  = get_theme_mod('behance_text');  
						$instagram  = get_theme_mod('instagram_text');  
						$linkedin  = get_theme_mod('linkedin_text');
						$gplus  = get_theme_mod('gplus_text');
						?>
						<?php if(!empty($facebook)){ ?>
						<li><a href="http://facebook.com/<?php echo esc_url($facebook);?>" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a></li>
						<?php } ?>
						<?php if(!empty($twitter)){ ?>
						<li><a href="http://twitter.com/<?php echo esc_url($twitter);?>" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a></li>
						<?php } ?>
						<?php if(!empty($vimeo)){ ?>
						<li><a href="<?php echo esc_url($vimeo);?>" target="_blank" class="vimeo"><i class="fa fa-vimeo-square"></i></a></li>
						<?php } ?>
						<?php if(!empty($behance)){ ?>
						<li><a href="<?php echo esc_url($behance);?>" target="_blank" class="behance"><i class="fa fa-behance-square"></i></a></li>
						<?php } ?>
						<?php if(!empty($dribbble)){ ?>
						<li><a href="<?php echo esc_url($dribbble);?>" target="_blank" class="dribbble"><i class="fa fa-dribbble"></i></a></li>
						<?php } ?>
						<?php if(!empty($pinterest)){ ?>
						<li><a href="<?php echo esc_url($pinterest);?>" target="_blank" class="pinterest"><i class="fa fa-pinterest"></i></a></li>
						<?php } ?>
						<?php if(!empty($instagram)){ ?>
						<li><a href="<?php echo esc_url($instagram);?>" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a></li>
						<?php } ?>
						<?php if(!empty($gplus)){ ?>
						<li><a href="<?php echo esc_url($gplus);?>" target="_blank" class="gplus"><i class="fa fa-google-plus"></i></a></li>
						<?php } ?>
						<?php if(!empty($flickr)){ ?>
						<li><a href="<?php echo esc_url($flickr);?>" target="_blank" class="flickr"><i class="fa fa-flickr"></i></a></li>
						<?php } ?>
						<?php if(!empty($linkedin)){ ?>
						<li><a href="<?php echo esc_url($linkedin);?>" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
						<?php } ?>
						<?php if(!empty($youtube)){ ?>
						<li><a href="<?php echo esc_url($youtube);?>" target="_blank" class="youtube"><i class="fa fa-youtube"></i></a></li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div>
		<a href="#top-page" class="scrolltotop" title="Up"></a>
	</section>
</footer>