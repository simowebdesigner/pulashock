<article <?php post_class(); ?>>
  <div class="content-post" data-vp-add-class="fadeIn animated" >
    <div class="row">
      <?php if ( has_post_thumbnail()): ?> 
      <div class="col-sm-4">
        <figure>
          <a class="link-post" href="<?php the_permalink(); ?>">
            <div class="overlay"></div>
            <?php echo get_the_post_thumbnail($post->ID, 'news-thumb', array('class' => 'img-responsive w_full')); ?>
          </a>	
        </figure>
      </div>
      <div class="col-sm-8">
        <div class="box-text">
          <header>
            <a href="<?php the_permalink(); ?>"><h3 class="entry-title mt-10"><?php the_title(); ?></h3></a>
            <?php get_template_part('templates/entry-meta'); ?>
          </header>
          <div class="entry-summary">
            <?php the_excerpt(); ?>
          </div>
          <?php echo '<a href="'.get_permalink().'" class="btn btn-readmore-link mt-0">'.__('Leggi tutto', 'franky').'</a>'; ?>
        </div>
      </div>
      <?php else: ?>
      <div class="col-sm-12">
        <div class="box-text">
          <header>
            <a href="<?php the_permalink(); ?>"><h3 class="entry-title mt-10"><?php the_title(); ?></h3></a>
            <?php get_template_part('templates/entry-meta'); ?>
          </header>
          <div class="entry-summary">
            <?php the_excerpt(); ?>
          </div>
          <?php echo '<a href="'.get_permalink().'" class="btn btn-readmore-link mt-0">'.__('Leggi tutto', 'franky').'</a>'; ?>
        </div>
      </div>
      <?php endif;?>
    </div>
  </div>
</article>