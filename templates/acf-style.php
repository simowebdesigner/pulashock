<?php if( class_exists('acf') ) : ?>
<?php if( have_rows('flexible_content') ): ?>
<?php $count_n = 1; ?>
<?php echo '<style>'; ?>
<?php while( have_rows('flexible_content') ): the_row(); ?>
<?php
//padding & margin
$margin_top = get_sub_field('margin_top');
$margin_top = ($margin_top || $margin_top == '0') ?  'margin-top:'. $margin_top .'px;' : '';
$margin_bottom = get_sub_field('margin_bottom');
$margin_bottom = ($margin_bottom || $margin_bottom == '0') ?  'margin-bottom:'. $margin_bottom .'px;' : '';	
$padding_top = get_sub_field('padding_top');
$padding_top = ($padding_top || $padding_top == '0') ?  'padding-top:'. $padding_top .'px;' : '';
$padding_bottom = get_sub_field('padding_bottom');
$padding_bottom = ($padding_bottom || $padding_bottom == '0') ?  'padding-bottom:'. $padding_bottom .'px;' : '';
//background
$style_bg = get_sub_field('background');
$background_color = get_sub_field('background_color'); 
$background_color = ($background_color) ?  'background-color:'. $background_color .';' : '';
$background_image = get_sub_field('background_image');
$background_image = ($background_image) ? 'background-image: url('. $background_image['url'] .');' : '';
$background_repeat = get_sub_field('background_repeat');
$background_repeat = ($background_repeat) ? 'background-repeat:' . $background_repeat . ';' : '';
$background_position = get_sub_field('background_position');
$background_position = ($background_position) ? 'background-position:' . $background_position . ';' : '';
$background_size = get_sub_field('background_size');
$background_size = ($background_size) ? 'background-size:' . $background_size . ';' : '';
$background_origin = get_sub_field('background_origin');
$background_origin = ($background_origin) ? 'background-origin:' . $background_origin . ';' : '';
$background = ($style_bg == 'image') ? $background_image . '' . $background_position . '' . $background_repeat . '' . $background_origin . '' . $background_size . '' . $background_color : $background_color;
//color
$color_text = get_sub_field('color_text');
$color_text = ($color_text) ?  'color:'. $color_text . ';' : '';
//style section 
$styleGeneric = $margin_top .''. $margin_bottom .''. $padding_top .''. $padding_bottom .'' . $color_text . ''. $background;
//style overlay 
$background_overlay = get_sub_field('background_overlay');
$background_overlay = ($background_overlay) ?  'background-color:'. $background_overlay .';' : '';
$overlay_opacity = get_sub_field('overlay_opacity');
$overlay_opacity = ($overlay_opacity) ?  'opacity:'. $overlay_opacity .';' : '';
if ($style_bg == 'image'):
$overlayStyle = $background_overlay . $overlay_opacity . ';';
else :
 $overlayStyle = '';
 endif;
 ?>
<?php echo '#flexible-section-' . get_row_index() . '{' . $styleGeneric . '} '; ?>
<?php echo '#flexible-section-' . get_row_index() . ' .parallax-overlay {' . $overlayStyle . '}';  ?>
<?php
/* ######## block title ######## */
if( get_row_layout() == 'block_title' ): ?>
<?php
$color_title = get_sub_field('title_color');
$color_title = ($color_title) ?  'color:'. $color_title . ';' : '';
$small_color_title = get_sub_field('small_color_title');
$small_title_color = ($small_color_title) ?  ' color:'. $small_color_title . ';' : '';
echo '#flexible-section-' . get_row_index() . ' .title-big, #flexible-section-' . get_row_index() . ' .title-divider-line {' . $color_title . '}';
echo '#flexible-section-' . get_row_index() . ' .small-title, #flexible-section-' . get_row_index() . ' .hr-custom span.hr-icon {' . $small_title_color . '}';
echo '#flexible-section-' . get_row_index() . ' .title-divider-line:after, #flexible-section-' . get_row_index() . ' .title-divider-line::after { background-color:' . $small_color_title . '}'; 
?>
<?php endif; ?>
<?php
/* ######## block icons ######## */
if( get_row_layout() == 'block_icons' ): ?>
<?php
$color_icons = get_sub_field('color_icons'); 
$color_icons = ($color_icons)? 'color:' . $color_icons . ';':'';
$background_icons = get_sub_field('background_icons'); 
$background_icons = ($background_icons) ? 'background-color:'. $background_icons . ';' :'background-color:transparent;'; 
$background_color_box = get_sub_field('background_color_box'); 
$background_color_box = ($background_color_box) ? 'background-color:'. $background_color_box . ';' :'background-color:transparent;'; 
echo '#flexible-section-' . get_row_index() . ' .box-icon {' . $color_icons . '' . $background_icons . '}';
echo '#flexible-section-' . get_row_index() . ' .box-content {'. $background_color_box. '}';
echo '#flexible-section-' . get_row_index() . ' h5 {'. $color_text. '}';
?>
<?php endif; ?>
<?php 
//responsive
$id_section = get_row_index();
if( have_rows('responsive_oprions') ) {
    while ( have_rows('responsive_oprions') ) : the_row();
        $breakpoint = get_sub_field('breakpoint');
        if ($breakpoint['min_width'] && $breakpoint['max_width']) :
            echo '@media screen and (max-width: '.$breakpoint["max_width"].') and (min-width:'.$breakpoint["min_width"].') {';
                $mt = $breakpoint['margin_top'];
                $mt = ($mt || $mt ==0) ? 'margin-top:' . $mt . 'px;' : '';
                $mb = $breakpoint['margin_bottom'];
                $mb = ($mb || $mb ==0) ? 'margin-bottom:' . $mb . 'px;': '';
                $pt = $breakpoint['padding_top'];
                $pt = ($pt || $pt ==0) ? 'padding-top:' . $pt . 'px;' : '';
                $pb = $breakpoint['padding_bottom'];
                $pb = ($pb || $pb ==0) ? 'padding-bottom:' . $pb . 'px;' : '';
                echo '#flexible-section-' . $id_section . ' {'.$mt.''.$mb.''.$pt.''.$pb.'}';
                echo '}';
       endif;
    endwhile;
}
?>
<?php endwhile; ?>
<?php echo '</style>'; ?>
<?php endif; ?>
<?php endif; ?>