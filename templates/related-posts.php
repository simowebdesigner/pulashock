<?php if (is_singular()) : ?>
<?php  //related post by tag
$check_related  = get_option('check_related_portfolio');
$number_related  = get_option('numbers_portfolio');
$number_related = ($number_related) ? $number_related : '3';
if ($check_related) : ?>
<?php
	$orig_post = $post;
global $post;
//echo $post;
$tags = wp_get_post_tags($post->ID);
if ($tags) : ?>
<section id="related-post">
	<div id="container-isotope-related" class="container-isotope-related container style_polaroid masonry-gutters">
	<?php	
   
	$tag_ids = array();
	foreach($tags as $individual_tag) 
	$tag_ids[] = $individual_tag->term_id;
	$i = 0;
	$args=array(
		'post_type' => array('post','recensioni','interviste','biografie'),
	'tag__in' => $tag_ids,
	'post__not_in' => array($post->ID),
	'posts_per_page'=> $number_related, // Numero degli articoli correlati da mostrare
	'ignore_sticky_posts'=>1
	);
	$my_query = new wp_query( $args );
	if( $my_query->have_posts()) : ?>
	<h3 class="title-related-post"><?php _e('Related posts', 'franky'); ?></h3>
	<div class="row justify-content-sm-center">
	<?php
	while( $my_query->have_posts() ) {
	$my_query->the_post(); ?>
			<div class="col-sm-3 fade-scroll" data-vp-add-class="fadeIn animated">
				<?php get_template_part('templates/related_single', 'portfolio'); ?>
			</div><!--end col-sm-3-->

	<?php } ?>
	<?php endif; ?>
	<?php
	$post = $orig_post;
	wp_reset_query();?>
	</div><!--end row-->
	</div><!--end container-->
</section>
<?php endif; ?>
<?php endif; ?>
<?php elseif (is_single()) : ?>
<?php  //related post by tag
$check_related  = get_option('check_related');
$number_related  = get_option('number_related');
$number_related = ($number_related) ? $number_related : '3';
if ($check_related) : ?>
<?php
$orig_post = $post;
global $post;
$tags = wp_get_post_tags($post->ID);
if ($tags) : ?>
<section id="related-post">
	<div class="container">
	<?php	
	$tag_ids = array();
	foreach($tags as $individual_tag) 
	$tag_ids[] = $individual_tag->term_id;
	$i = 0;
	$args=array(
		'post_type' => 'post',
	'tag__in' => $tag_ids,
	'post__not_in' => array($post->ID),
	'posts_per_page'=> $number_related, // Numero degli articoli correlati da mostrare
	'ignore_sticky_posts'=>1
	);
	$my_query = new wp_query( $args );
	if( $my_query->have_posts()) : ?>
	<h3 class="title-related-post"><?php _e('Related posts', 'franky'); ?></h3>
	<?php
	while( $my_query->have_posts() ) {
	$my_query->the_post(); ?>
	<?php if ($i == 0) : ?>
		<div class="row justify-content-sm-center">
			<?php endif; ?>
			<div class="col-sm-3 fade-scroll" data-vp-add-class="fadeIn animated">
				<?php get_template_part('templates/content'); ?>
			</div><!--end col-sm-4-->
			<?php $i++; if($i % 3 == 0) : ?>
		</div><!--end row--><div class="row">
		<?php endif; ?>
	<?php } ?>
	<?php endif; ?>
	<?php
	$post = $orig_post;
	wp_reset_query();?>
	</div><!--end row-->
	</div><!--end container-->
</section>
<?php endif; ?>
<?php endif; ?>
<?php endif; ?>