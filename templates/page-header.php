<?php use Roots\Sage\Titles; ?>
<?php
//page single
?>
<?php if ( is_singular('portfolio')) : ?>
<?php $show_image_portfolio = get_option('check_image_portfolio'); ?>
<?php if ($show_image_portfolio) : ?>
<?php if ( has_post_thumbnail()): ?>
<?php $styleSlide =  'background-image:url(' . get_the_post_thumbnail_url() . ')'; ?>
<div id="content-slide">
  <div class="slide-header">
    <div class="swiper-container swiper-container-slide">
      <div class="swiper-wrapper">
        <div class="swiper-slide swiper-slide-active slide-parallax text-center" <?php echo 'style="' . esc_html($styleSlide) . '"';?>>
          <div class="overlay" style="background-color:#000; opacity:0.4;"></div>
          <div class="caption-tex">
            <div data-enllax-ratio="-0.1" data-enllax-type="foreground">
              <h1>
                <span class="small-single-title fadeAnimation"><?php the_title(); ?></span>
              </h1>
            </div>
          </div>
          <div class="preloader"></div>
        </div>
      </div>
    </div>
  </div><!--end Slide header-->
</div><!--end content-slide-->
<?php endif; ?>
<?php endif; ?>
<div class="page-block-header">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
          <?php if(function_exists('bcn_display'))
{
  bcn_display();
}?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php elseif (is_single() ): ?>
<?php //check image header post
$show_image = get_option('check_image'); ?>
<?php if ($show_image) : ?>
<?php if ( has_post_thumbnail()): ?>
<?php $styleSlide =  'background-image:url(' . get_the_post_thumbnail_url() . ')'; ?>
<div id="content-slide">
  <div class="slide-header">
    <div class="swiper-container swiper-container-slide">
      <div class="swiper-wrapper">
        <div class="swiper-slide swiper-slide-active slide-parallax text-center" <?php echo 'style="' . esc_html($styleSlide) . '"';?>>
          <div class="overlay" style="background-color:#000; opacity:0.4;"></div>
          <div class="caption-tex">
            <div data-enllax-ratio="-0.1" data-enllax-type="foreground">
              <h1>
                <span class="small-single-title fadeAnimation"><?php the_title(); ?></span>
              </h1>
            </div>
          </div>
          <div class="preloader"></div>
        </div>
      </div>
    </div>
  </div><!--end Slide header-->
</div><!--end content-slide-->
<?php else : ?>
<script>
  var b = document.getElementsByTagName("BODY")[0];
  b.classList.add("no-header-image");
</script>
<?php endif; ?>
<div class="page-block-header">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
          <?php if(function_exists('bcn_display'))
{
  bcn_display();
}?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php else : //not $show_image ?>
<div class="page-block-header">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
          <?php if(function_exists('bcn_display'))
{
  bcn_display();
}?>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  var b = document.getElementsByTagName("BODY")[0];
  b.classList.add("no-header-image");
</script>
<div class="container hide" style="display:none;">
  <div class="row">
    <div class="col-sm-12">
      <h1 class="single-title-post">
        <span class="title-slide fadeAnimation"><?php the_title(); ?></span>
      </h1>

    </div>
  </div>
  <div class="row">
    <div class="col-sm-12 entry-tags">
      <div class="row">
        <div class="col-sm-12">
          <?php the_tags( 'Tags: ', '' ); ?>
        </div>
        
      </div>
    </div>
  </div>
</div>
<?php endif; ?>
<?php else : //not single ?>
<div class="page-block-header">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
          <?php if(function_exists('bcn_display'))
{
  bcn_display();
}?>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container pb-40">
  <div class="row">
    <div class="col-sm-12">
      <?php $title = Titles\title(); ?>
      <h1 class="title-header"><?php echo $title; ?></h1>
    </div>
  </div>
</div>
<script>
  var b = document.getElementsByTagName("BODY")[0];
  b.classList.add("no-header-image");
</script>
<?php endif; ?>