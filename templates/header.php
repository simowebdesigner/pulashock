<header class="banner">
	<!--top menu-->
	<div class="content-top-menu">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<?php $franky_logo  = get_theme_mod('logo_image'); ?>
					<div class="logo <?php if(empty($franky_logo)) : ?>logo-title<?php endif; ?>"> 
						<?php if(!empty($franky_logo)) { ?>
						<a class="brand" href="<?php echo esc_attr(home_url()); ?>"> <img class="logo-top" src="<?php echo esc_attr($franky_logo);?>" alt=""></a>
						<?php }
						else { ?>
						<span class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></span>
						<p class="site-description"><?php bloginfo( 'description' ); ?></p>
						<?php } ?>
					</div>
					<?php // button top var
					$search_button = get_option('check_button_search');
					$show_button = get_option('check_button');
					$button_link = get_option('url_button'); 
					$button_text = get_option('text_button'); 
					?>
					<ul class="list-button-menu">
						<?php if ($show_button) : ?>
						<?php if ($button_text) : ?>
						<li>
							<a href="<?php echo esc_html('tel:'.$button_link); ?>" class="button_custom"><?php echo esc_html($button_text); ?></a>
						</li>
						<?php endif; ?>
						<?php endif; ?>
						<?php if ($search_button) : ?>
						<li>
							<a href="#" class="button_search"><i class="fa fa-search"></i></a>
							<div class="content-top-search-form">
								<?php get_template_part('templates/searchform','top');?>
							</div>
						</li>
						<?php endif; ?>
					</ul>
					<div class="top-menu">
						<?php
						if (has_nav_menu('primary_navigation')) :
						wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => '']);
						endif;
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--/top menu-->
</header>
<div class="container">
	<?php 
	/*

array(1) {
[0]=> array(5) { 
["blockName"]=> string(14) "core/paragraph" 
["attrs"]=> array(0) { } 
["innerBlocks"]=> array(0) { } 
["innerHTML"]=> string(9) "
"["innerContent"]=> array(1) { [0]=> string(9) "

" } } } 


	*/
/*
	$post = get_post(); 
	if ( has_blocks( $post->post_content ) ) {
		echo '<div style="margin-top:100px;"></div>';
		$characters = parse_blocks( $post->post_content );
//var_dump($characters);
		foreach ($characters[0] as $obj_key => $obj) {
			
			if($obj_key === "attrs") {
				if($obj['id'] === 'id') {
				echo "id: ";
				echo ' <strong>value:</strong> '; 
				print_r($obj['id']);
				echo '<br>';
				}
				echo "id: ";
				echo ' <strong>value:</strong> '; 
				print_r($obj['id']);
				echo '<br>';
				$v = $obj;
				foreach ((array) $v as $key => $value) {
					print_r($key);
					echo' : ';
					print_r($value);
					echo '<br>';
					$p = $value;
					foreach ((array) $p as $key => $value) {
						print_r($key);
						echo' : ';
						print_r($value);
						echo '<br>';
					}
				}
			}  

		}
	}*/
	?>
</div>
