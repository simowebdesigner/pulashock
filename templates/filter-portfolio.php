<?php 
if ( has_post_thumbnail()) {
$thumb_id = get_post_thumbnail_id();
$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'portfolio-size', true);
$thumb_url = $thumb_url_array[0]; 
}
else {
$thumb_url = 'https://place-hold.it/500';
}
?>
<div class="box-img-portfolio">
<?php echo get_the_post_thumbnail($post->ID, 'large', array('class' => 'img-responsive w_full')); ?>
</div>
	<div class="box" >
		<div class="caption-box">
			<div class="icon-box">
				<div class="caption-description">
					<h4 class="caption-title"><?php the_title(); ?></h4>
					<div class="italic"><?php the_category(', '); ?></div>
					<div class="caption-text"><?php // echo substr(get_the_excerpt(), 0,60); ?></div>
				</div>
				<a class="icon-link" href="<?php the_post_thumbnail_url( 'large' );  ?>" data-lightbox="image-1" data-title="<?php the_title(); ?>"><i>+</i></a>
				<a class="icon-link" href="<?php the_permalink(); ?>"><i class="fa fa-link" aria-hidden="true"></i>
				</a>
			</div>
		</div>
	</div><!--end box-->