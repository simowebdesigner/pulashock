<div class="humburger-menu">
	<div id="header-controls-right">
		<!--navigation-->
		<div class="navigation-humburger-menu">
			<span class="first"></span>
			<span class="second"></span>
			<span class="thirt"></span>
		</div>
	</div><!--/header-controls-right-->
	<?php $franky_logo  = get_theme_mod('logo_image_mobile'); 
if(!empty($franky_logo)) { ?>
	<a class="brand" href="<?php echo esc_attr(home_url()); ?>"> <img class="logo-top" src="<?php echo esc_attr($franky_logo);?>" alt=""></a>
	<?php }
else { ?>
	<span class="site-title"><a href="<?php echo esc_url(home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></span>
	<p class="site-description"><?php bloginfo( 'description' ); ?></p>
	<?php } ?>
	<?php if (has_nav_menu('primary_navigation')) :
wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav','menu_id' => 'humburger-top-menu']);
endif; ?>
	<ul class="social-icons text-center">
		<?php 
		$twitter  = get_theme_mod('twitter_text');  
		$facebook  = get_theme_mod('facebook_text');  
		$dribbble  = get_theme_mod('dribbble_text');  
		$pinterest  = get_theme_mod('pinterest_text');  
		$vimeo  = get_theme_mod('vimeo_text');  
		$flickr  = get_theme_mod('flickr_text');  
		$youtube  = get_theme_mod('youtube_text');  
		$behance  = get_theme_mod('behance_text');  
		$instagram  = get_theme_mod('instagram_text');  
		$linkedin  = get_theme_mod('linkedin_text');
		$gplus  = get_theme_mod('gplus_text');
		?>
		<?php if(!empty($facebook)){ ?>
		<li><a href="http://facebook.com/<?php echo esc_attr($facebook);?>" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a></li>
		<?php } ?>
		<?php if(!empty($twitter)){ ?>
		<li><a href="http://twitter.com/<?php echo esc_attr($twitter);?>" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a></li>
		<?php } ?>
		<?php if(!empty($vimeo)){ ?>
		<li><a href="<?php echo esc_url($vimeo);?>" target="_blank" class="vimeo"><i class="fa fa-vimeo-square"></i></a></li>
		<?php } ?>
		<?php if(!empty($behance)){ ?>
		<li><a href="<?php echo esc_url($behance);?>" target="_blank" class="behance"><i class="fa fa-behance-square"></i></a></li>
		<?php } ?>
		<?php if(!empty($dribbble)){ ?>
		<li><a href="<?php echo esc_url($dribbble);?>" target="_blank" class="dribbble"><i class="fa fa-dribbble"></i></a></li>
		<?php } ?>
		<?php if(!empty($pinterest)){ ?>
		<li><a href="<?php echo esc_url($pinterest);?>" target="_blank" class="pinterest"><i class="fa fa-pinterest"></i></a></li>
		<?php } ?>
		<?php if(!empty($instagram)){ ?>
		<li><a href="<?php echo esc_url($instagram);?>" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a></li>
		<?php } ?>
		<?php if(!empty($gplus)){ ?>
		<li><a href="<?php echo esc_url($gplus);?>" target="_blank" class="gplus"><i class="fa fa-google-plus"></i></a></li>
		<?php } ?>
		<?php if(!empty($flickr)){ ?>
		<li><a href="<?php echo esc_url($flickr);?>" target="_blank" class="flickr"><i class="fa fa-flickr"></i></a></li>
		<?php } ?>
		<?php if(!empty($linkedin)){ ?>
		<li><a href="<?php echo esc_url($linkedin);?>" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
		<?php } ?>
		<?php if(!empty($youtube)){ ?>
		<li><a href="<?php echo esc_url($youtube);?>" target="_blank" class="youtube"><i class="fa fa-youtube"></i></a></li>
		<?php } ?>
	</ul>
</div><!--end humburger-menu-->