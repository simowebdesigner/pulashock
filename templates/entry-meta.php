<p class="meta-news small mb-0">
<?php $author = get_the_author(); ?>
<?php $url = admin_url(); ?>
<?php $date = get_the_date(); ?>
<?php $time = get_post_time('c', true); ?>
	<time class="updated" datetime="<?php echo esc_html($time); ?>"><span><?php echo esc_html($date); ?></span></time>
	<span><?php _e('in', 'franky'); ?></span> <?php the_category(', '); ?>
	<span><?php _e('by', 'franky'); ?></span> <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" rel="author" class="fn"><?php the_author_meta( 'display_name' ); ?></a>
</p>