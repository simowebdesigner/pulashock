/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {
  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
        var menu = $('.content-top-menu');
        // top menu fixed on scroll
        $(window).scroll(function() {
          // 100 = The point you would like to fade the nav in.
          if ($(window).scrollTop() > 20) {
            $(menu).addClass('fixed');
            $('.humburger-menu').addClass('humburger-fixed');
          } else {
            $(menu).removeClass('fixed');
            $('.humburger-menu').removeClass('humburger-fixed');
          }
        });


        /************************
				Humburger-menu
				************************/
        $(".navigation-humburger-menu").click(function() {
          $('body').toggleClass("open-humburger-menu");
        });

        $("#humburger-top-menu .menu-item-has-children > a").click(function(e) {
          e.preventDefault();
          $(this).parents('li').find('.sub-menu').slideToggle('100');
        });
        // Search
        // -------------------------------------------------------
        $(".list-button-menu .button_search").click(function(e) {
          e.preventDefault();
          $('.content-top-search-form').toggleClass("search-open");
        });

        /* Swiper carousel
 				************************************/
        var swiper = new Swiper('.gallery-carousel', {
          pagination: '.swiper-pagination',
          nextButton: '.swiper-button-next',
          prevButton: '.swiper-button-prev',
          slidesPerView: 4,
          //centeredSlides: true,
          paginationClickable: true,
          spaceBetween: 30,
          breakpoints: {
            768: {
              slidesPerView: 2,
              spaceBetween: 30
            },
            640: {
              slidesPerView: 2,
              spaceBetween: 20
            },
            320: {
              slidesPerView: 2,
              spaceBetween: 10
            }
          }
        });

        /************************
				Opacity on scroll
				************************/
        function opacityScroll() {
          jQuery(window).scroll(function(){
            if ( jQuery(window).innerWidth() > 10 ) {
              var scrollVar = jQuery(window).scrollTop();
              jQuery('.slide-header').css('opacity', 1 - (jQuery(window).scrollTop()) / ((jQuery(window).height())) );
            }
          });
        }
        opacityScroll();
        /************************
				Scroll top
		     ************************/

        $(window).scroll(function() {
          if ($(this).scrollTop() > 250) {
            $('.scrolltotop').fadeIn();
          } else {
            $('.scrolltotop').fadeOut();
          }
        });
        $('.scrolltotop').click(function() {
          $("html, body").animate({
            scrollTop: 0
          }, 800);
          return false; 
        });

        /************************
				Anchor
				************************/
        function anchorScrollSlide() {
          $('.readmore-slide a[href^="#"]').click(function() { 
            var height = jQuery('.content-top-menu').height() -10;
            var target = $(this.hash);  
            $('html, body').animate({ scrollTop: target.offset().top - height }, 500);  
            return false;  
          });  
        }
        anchorScrollSlide();
        //var templateDir = SiteParameters.site_url;
        if (!jQuery('body').hasClass("home")) {
          var templateDir = SiteParameters.site_url;
          jQuery('.content-top-menu ul li a[href*="#"]').each(function() {
            var anchor = jQuery(this).attr("href");
            //console.log(anchor);
            //console.log(templateDir);
            jQuery(this).attr("href", templateDir+'/'+anchor);
          });
        }

        function anchorScrollMenu() {
          $('.content-top-menu ul > li > a[href^="#"]').click(function() { 
            var height = jQuery('.content-top-menu').height() -10;
            var target = $(this.hash);  
            $('html, body').animate({ scrollTop: target.offset().top - height }, 500);  
            return false;  
          }); 
        }
        anchorScrollMenu();

        function anchorScrollMobile() {
          $('.humburger-menu a[href^="#"]').click(function() { 
            var height = jQuery('.content-top-menu').height() -10;
            var target = $(this.hash);  
            $('html, body').animate({ scrollTop: target.offset().top - height }, 500);  
            return false;  
          }); 
        }
        anchorScrollMobile();

        /************************
				Accordion
				************************/
        function accordionCustom() {
          $('.collapse').on('shown.bs.collapse', function(){
            $(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
          }).on('hidden.bs.collapse', function(){
            $(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
          });
        }
        accordionCustom();

        /************************
				Lazy images
				************************/

        $('.lazy').Lazy({
          // your configuration goes here
          scrollDirection: 'vertical',
          effect: 'fadeIn',
          visibleOnly: true,
          onError: function(element) {
            console.log('error loading ' + element.data('src'));
          }
        });
        /************************
				Preload
				************************/
        /*$(".animsition").animsition({
          inClass: 'fade-in',
          outClass: 'fade-out',
          inDuration: 1500,
          outDuration: 800,
          linkElement: '.animsition-link',
          // e.g. linkElement: 'a:not([target="_blank"]):not([href^="#"])'
          loading: true,
          loadingParentElement: 'body', //animsition wrapper element
          loadingClass: 'animsition-loading',
          //loadingInner: '', // e.g '<img src="loading.svg" />'
          timeout: false,
          timeoutCountdown: 5000,
          onLoadEvent: true,
          browser: [ 'animation-duration', '-webkit-animation-duration'],
          // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
          // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
          overlay : false,
          overlayClass : 'animsition-overlay-slide',
          overlayParentElement : 'body',
          transition: function(url){ window.location.href = url; }
        })*/
        /* Parallax enllax
				************************************/
        $(window).enllax({

          //default values

          // type can be defined as it is background parallax scrolling element or foreground scrolling element.
          //type: 'background',  // another value for type is 'foreground'.

          ratio: 0.1,  // multiplier for scrolling speed. Less is slower. Default: '0'.

          direction: 'vertical' // another value for direction is 'horizontal'.

        });


        /* viewportChecker
     		************************************/
        //amimate fade
        jQuery('.fade-scroll').addClass("hidden-scroll").viewportChecker();
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    },
    // About us page, note the change from about-us to about_us.
   // Note the change from about-us to about_us.
  'post_type_archive_work': {
    init: function() {
      console.log('active');
    }
  }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
