
<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'franky'); ?>
  </div>
<?php endif; ?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
<?php endwhile; ?>

<?php // the_posts_navigation(); ?>
<div class="col-sm-12 text-center">
	<?php the_posts_pagination( array(
	'mid_size' => 2,
	'prev_text' => __( '<i class=\'fa fa-long-arrow-left\' aria-hidden="true"> </i>', 'franky' ),
	'next_text' => __( '<i class=\'fa fa-long-arrow-right\' aria-hidden="true"> </i>', 'franky' ),
) ); ?>
</div>
