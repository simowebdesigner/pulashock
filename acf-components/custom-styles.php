<?php /************************************
			Global and typography
			************************************/
//var 
$color_text = esc_attr(get_theme_mod('text_color'));
$color_link = esc_attr(get_theme_mod('primary_color'));
$color_link_hover = esc_attr(get_theme_mod('link_color_hover'));
$color_title = esc_attr(get_theme_mod('title_color'));
$custom_css = esc_attr(get_theme_mod('custom_theme_css'));
$custom = ($custom_css)? $custom_css : '';
$button_style = esc_attr(get_option('select_header'));
$body_color = ($color_text)? $color_text : '#666666';
$link_color = ($color_link)? $color_link : '#FF756A';
$link_color_hover = ($color_link_hover)? $color_link_hover : '#666';
$title_color = ($color_title)? $color_title : '#141414';
if ($button_style == "rounded") {
	$button_radius = '20px';
}
else {
	$button_radius = '0';
}
$style_button = ($button_style) ? $button_radius : '10px';
?>
<?php $custom_inline_style = '
		/*generic*/
		body {	
			color: '. $body_color .';
		}
		h1,
		h2,
		h3,
		h4,
		h5,
		h6,
		.block-title .title-big, 
		.logo.logo-title, 
		.logo.logo-title a {
			color:'.$title_color.';
		}
		.block-title .small-title {
			color:'.$link_color.';
		}
		.title-divider-line:after {
			background-color:'.$link_color.';
		}
		a:link,
		a:visited,
		.btn-link {
			color:'.$link_color.';
		}
		a:hover,
		a:active,
		a:focus,
	 .btn-link:hover,
		.btn-link:active,
		.btn-link:focus {
			color:'.$link_color_hover.';
		}
		/*menu*/
		/*custom button*/
		.content-top-menu.fixed .top-menu ul li > a,
		.no-header-image .content-top-menu .top-menu ul li > a {
			color:'.$title_color.';
		}
		.content-top-menu .list-button-menu li a.button_search {
			color:'.$link_color.';
		}
		.content-top-menu .list-button-menu li a.button_custom:active,
		.content-top-menu .list-button-menu li a.button_custom:focus,
		.content-top-menu .list-button-menu li a.button_custom:hover {
			color:'.$link_color.';
			background-color:transparent;
		}
		.content-top-menu .list-button-menu li a.button_custom {
			background-color:'.$link_color.';
		}
		.content-top-menu .list-button-menu li a.button_search:active,
		.content-top-menu .list-button-menu li a.button_search:focus,
		.content-top-menu .list-button-menu li a.button_search:hover {
			color:'.$link_color.';
		}
		/*top menu*/
		.content-top-menu .top-menu ul li.current-menu-parent > a,
		.content-top-menu .top-menu ul li a:active,
		.content-top-menu .top-menu ul li a:focus,
		.content-top-menu .top-menu ul li a:hover {
			color:'.$link_color.';
		}
		.content-top-menu .top-menu ul li.active a,
		.content-top-menu .top-menu ul li.current-menu-item > a {
			color:'.$link_color.';
		}
		.content-top-menu .top-menu ul li.active a:active,
		.content-top-menu .top-menu ul li.active a:focus,
		.content-top-menu .top-menu ul li.active a:hover,
		.content-top-menu .top-menu ul li.current-menu-item > a:active,
		.content-top-menu .top-menu ul li.current-menu-item > a:focus,
		.content-top-menu .top-menu ul li.current-menu-item > a:hover {
			color:'.$link_color.';
		}
		/*sub-menu*/
		.content-top-menu .top-menu ul li ul.sub-menu li a {
			color:' .$body_color. ';
		}
		.content-top-menu .top-menu ul li ul.sub-menu li a:hover,
		.content-top-menu .top-menu ul li ul.sub-menu li a:focus,
		.content-top-menu .top-menu ul li ul.sub-menu li.current-menu-item a,
		.content-top-menu .top-menu ul li ul.sub-menu li.current-menu-item a:hover,
		.content-top-menu .top-menu ul li ul.sub-menu li.current-menu-item a:focus {
			color:#fff;
			background-color: '.$link_color.';
		}
		/*humburger menu*/
		.humburger-menu ul.nav li.menu-item-has-children > a::after {
			content: " +";
			color:'.$link_color.';
		}
		.humburger-menu ul.nav > li a:active, .humburger-menu ul.nav > li a:focus, .humburger-menu ul.nav > li a:hover {
			color:'.$link_color.';
			background-color: transparent;
		}
		.humburger-menu ul.nav li.current-menu-item > a {
			color:'.$link_color.';
		}
		/*button btn*/
		.btn, .comment-form input[type="submit"], .search-form .search-submit {
			border-radius: '.$style_button.';
		}
		.btn-readmore,
		button.btn-readmore,
		a.btn-readmore {
			background-color: '.$link_color.';
			color:#fff !important;
		}
		.btn-readmore:active:focus, 
		.btn-readmore:active:hover, 
		.btn-readmore:focus, 
		.btn-readmore:hover, 
		a.btn-readmore:active:focus, 
		a.btn-readmore:active:hover, 
		a.btn-readmore:focus, 
		a.btn-readmore:hover
		button.btn-readmore:hover {
			background-color: #fff;
			color:'.$link_color.' !important;
			border-color:'.$link_color.';
		}
		.btn-empty:hover,
		a.btn-empty:hover {
			background-color: #fff;
			color:'.$link_color.' !important;
		}
		.btn-primary,
		button.alm-load-more-btn, 
		button.alm-load-more-btn.done,
		.comment-form input[type="submit"] {
			background-color:'.$link_color.';
			border-color: '.$link_color.';
			color: #fff;
		}
		
		.btn-primary.active, 
		.btn-primary:active, 
		.btn-primary:hover, 
		button.alm-load-more-btn:hover,
		button.alm-load-more-btn:focus,
		button.alm-load-more-btn:active,
		.comment-form .open > input.dropdown-toggle[type="submit"], 
		.comment-form input.active[type="submit"], 
		.comment-form input[type="submit"]:active, 
		.comment-form input[type="submit"]:hover, 
		.open > .btn-primary.dropdown-toggle {
			background-color:'.$link_color.';
			border-color: '.$link_color.';
			color: #fff !important;
		}
		/*slide - carousel - gallery*/
		#content-slide .swiper-container-slide.swiper-container-slide .swiper-wrapper .swiper-slide .caption-tex .title-slide span {
			color:'.$link_color.';
		}
		.swiper-container.gallery-carousel .swiper-pagination-bullet-active {
			background: '.$link_color.' none repeat scroll 0 0;
		}
		.block_carousel .swiper-pagination .swiper-pagination-bullet-active, 
		.swiper-container.swiper-container-carousel .swiper-pagination .swiper-pagination-bullet-active {
			background: '.$link_color.' none repeat scroll 0 0;
		}
		/*tabs*/
		.block-tabs .nav-tabs > li > a.active, 
		.block-tabs .nav-tabs > li > a.active:focus, 
		.block-tabs .nav-tabs > li > a.active:hover {
			background-color: '.$link_color.';
			color: #fff;
			border-color: '.$link_color.';
		}
		/*style default*/
		.block-tabs .default-tabs .nav-tabs > li > a.active, 
		.block-tabs .default-tabs .nav-tabs > li > a.active:focus, 
		.block-tabs .default-tabs .nav-tabs > li > a.active:hover {
			background-color: '.$link_color.';
			border-color: '.$link_color.';
			color: #fff;
		}
		/*style line*/
		.block-tabs .line-tabs .content-nav-line .nav-tabs-line li.active .border-after  {
			background-color: '.$link_color.';
		}
		.block-tabs .content-nav-line .nav-tabs-line li a:hover,
		.block-tabs .content-nav-line .nav-tabs-line li a:focus,
		.block-tabs .content-nav-line .nav-tabs-line li a:active {
			color: '.$link_color.';
		}

		/*accordion*/
		.block-accordions .panel .accordion-toggle h4 {
			color: '.$link_color.';
		}	
		.block-accordions .panel .accordion-toggle.collapsed h4 {
			color: '.$color_text.';
		}

		/*alternate*/
		.block-alternate .content-text .box-text::before {
						background-color: '.$link_color.';
		}

		/*hr custom*/
		.hr-custom span.hr-icon {
			color: '.$link_color.';
		}
		'
?>