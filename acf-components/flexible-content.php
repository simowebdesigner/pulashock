<?php if( have_rows('flexible_content') ): ?>
<?php $count_n = 1; ?>
<?php while( have_rows('flexible_content') ): the_row(); ?>
<?php //include block title
get_template_part('acf-components/include/block_title');
//include block icons
//get_template_part('acf-components/include/block_icons');
 //include block alternate
get_template_part('acf-components/include/block_alternate');
//include block news
get_template_part('acf-components/include/block_news');
//include call to action
get_template_part('acf-components/include/call_to_action');
//include block info icons
//get_template_part('acf-components/include/block_info_icons');
//include block icons lists
//get_template_part('acf-components/include/block_icons_lists');
//include block portfolio
//get_template_part('acf-components/include/block_portfolio');
//include block masonry
get_template_part('acf-components/include/block_masonry');
//include block clients
get_template_part('acf-components/include/block_clents');
//include block carousel
get_template_part('acf-components/include/block_carousel');
//include block map
//get_template_part('acf-components/include/block_map');
//include block tabs
//get_template_part('acf-components/include/block_tabs');
 //include block accordions
//get_template_part('acf-components/include/block_accordions');
 //include block text
get_template_part('acf-components/include/block_text');
//include block skills
get_template_part('acf-components/include/block_skills');
//include block pages
get_template_part('acf-components/include/block_pages');
 //include block numbers
get_template_part('acf-components/include/block_numbers');
 //include columns
get_template_part('acf-components/include/block_columns');
//include divider
get_template_part('acf-components/include/block_divider');
?> 
<?php endwhile; ?>
<?php endif; ?>