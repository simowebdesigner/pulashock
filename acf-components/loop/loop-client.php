<?php 
if( get_row_layout() == 'block_clients' ) {
$link_target = get_sub_field('link_target');
$link_target = ($link_target) ? $link_target : '_self';
}
$url_client = get_field('url_client');
if ( has_post_thumbnail()) : ?>
<div class="swiper-slide">
	<?php if ($url_client) : ?><a href="<?php echo esc_html($url_client); ?>" target="<?php echo esc_html($link_target); ?>" title="<?php the_title(); ?>"><?php endif; ?>
	<div class="fade-scroll box-img" data-vp-add-class="fadeIn animated">
		<?php the_post_thumbnail( 'medium', array( 'class'  => 'img-responsive' ) ); ?>
	</div>
	<?php if ($url_client) : ?></a><?php endif; ?>
</div>
<?php endif; ?>