<?php 
if( get_row_layout() == 'block_carousels' ) {
$link_target = get_sub_field('link_target');
$link_target = ($link_target) ? $link_target : '_self';
}

if( have_rows('gallery')): ?>
<?php while( have_rows('gallery') ): the_row(); 
// vars
$text = get_sub_field('text');
$link = get_sub_field('link');
$image = get_sub_field('image');
		?>
<div class="swiper-slide">
	<figure>
	<?php if ($link) : ?><a href="<?php echo esc_html($link); ?>" title="<?php the_title(); ?>" target="<?php echo esc_html($link_target); ?>"><?php endif; ?>
	<div class="overlay"></div>
	<?php if( !empty($image) ): ?>
	<div class="fade-scroll box-img" data-vp-add-class="fadeIn animated">
		<?php echo wp_get_attachment_image(get_sub_field('image'), 'portfolio-size', 0, array('class' => 'img-responsive')); ?>
	</div>
	<?php if ($link) : ?></a><?php endif; ?>
	</figure>
	<?php endif; ?>
		<?php if ($text) : ?>
		<div class="block-text fade-scroll" data-vp-add-class="fadeIn animated">
			<?php the_sub_field('text');?>
		</div>
		<?php endif; ?>
	
	<p><?php echo esc_html($image['caption']); ?></p>
</div>
<?php endwhile; ?>
<?php endif; ?>