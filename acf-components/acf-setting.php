<?php
/*******************
ACF framework
*******************/

/*******************
Include ACF theme
*******************/
// 3. Hide ACF field group menu item
//add_filter('acf/settings/show_admin', '__return_true');


// 4. Include ACF
//include_once( get_stylesheet_directory() . '/plugin/advanced-custom-fields-pro/acf.php' );

/*******************
Custom admin css ACF
*******************/
add_action('acf/input/admin_head', 'my_acf_admin_head');
function my_acf_admin_head() {
	?>
	<style type="text/css">
		.acf-tab-group li a {
			text-transform:uppercase;
			padding:5px 20px;
			border-radius:  0;
		}
		.acf-tab-group li.active a,.acf-tab-wrap.-left .acf-tab-group li.active a   {
			color:#D05353;
		}

		/*title label options*/
		.title-section label {
			font-size:20px !important;
			padding-top:30px !important;
			/*border-top:1px solid #ddd;*/
		}
		.title-section.title-first label{
			margin-top:0 !important;
			border-top-width:0;
		}
		clear {
			clear:both;
		}
</style>
	<?php
}


add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
  echo '<style>
    #update-nag, .update-nag {
		display: block;
	}
	.clear {
		clear:both !important;
	}
  </style>';
}

//add_action('acf/input/admin_head', 'my_acf_admin_head_flex');


/*********
Acf fields
**********/

