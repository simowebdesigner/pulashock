<?php //slide
if( class_exists('acf') ) {
$post_object = get_field('select_slide');
if( $post_object ): 
// override $post
$post = $post_object;
setup_postdata( $post ); 
//var
$slide = have_rows('slider'); 
$navigation = get_field('navigation');
$pagination = get_field('pagination');
$effect = get_field('effect');
$time = get_field('time');
$autoplay = get_field('autoplay');
$speed = get_field('speed_transition'); ?>
<script type="text/javascript" defer="defer">
	<?php if($slide): ?>
	jQuery(document).ready(function ($) {
		/************************
			Slide
			************************/
		var mySwiper = new Swiper('.swiper-container-slide', {
			<?php // Optional parameters ?> 
			<?php //pagination ?>
			<?php if ($pagination) : ?>
			pagination: '.swiper-pagination',
			paginationClickable: true,
			<?php endif; ?> 
			<?php if ($navigation) : ?>
			nextButton: '.swiper-button-next',
			prevButton: '.swiper-button-prev',
			<?php endif; ?>
			centeredSlides: true,
			<?php //effect ?>
			<?php if ($effect =="fade") : ?>
			effect: 'fade',
			<?php endif; ?> 
			<?php //time ?>
			<?php if ($autoplay =="true") : ?>
			autoplayDisableOnInteraction: false,
			autoplay: <?php echo esc_html($time); ?>,
			<?php endif; ?> 
			spaceBetween: 0,
			preloadImages: true,
			<?php if ($speed) { ?>
			speed:<?php echo esc_html($speed); ?>
			<?php } ?>
		}); 
		$('.swiper-slide').on('mouseover',function() {
			mySwiper.stopAutoplay();
		});
		$('.swiper-slide').on('mouseout',function() {
			mySwiper.startAutoplay();
		});
	});
	<?php endif; ?>
	<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
</script>
<?php endif; ?>
<?php /************************************************
	flexi content
************************************************/?>
<?php if( have_rows('flexible_content') ): ?>
<?php $count_n = 1; ?>
<?php while( have_rows('flexible_content') ): the_row(); ?>
<?php $count_n++; ?>
<?php if( get_row_layout() == 'block_carousel' ): ?>
<?php $navigation = get_sub_field('navigation');
$pagination = get_sub_field('pagination');
$time = get_sub_field('time');
$autoplay = get_sub_field('autoplay');
$column = get_sub_field('column'); ?>
<script type="text/javascript">
	jQuery(document).ready(function ($) {
		/************************
			Slide
			************************/
		var mySwiper = new Swiper('#block_carousel<?php echo esc_html($count_n); ?>', {
			<?php // Optional parameters ?> 
			<?php //pagination ?>
			<?php if ($pagination) : ?>
			pagination: '.swiper-pagination',
			paginationClickable: true,
			<?php endif; ?> 
			<?php if ($navigation) : ?>
			nextButton: '.swiper-button-next',
			prevButton: '.swiper-button-prev',
			<?php endif; ?>
			slidesPerView: <?php echo	esc_html($column); ?>,
			<?php //time ?>
			<?php if ($autoplay =="true") : ?>
			autoplayDisableOnInteraction: false,
			autoplay: <?php echo esc_html($time); ?>,
			<?php endif; ?> 
			spaceBetween: 0,
			preloadImages: true,
			breakpoints: {
			640: {
			slidesPerView: 1,
			spaceBetween: 20
		},
																												320: {
																												slidesPerView: 1,
																												spaceBetween: 10
																												}
																												}
																												}); 
	});
</script>
<?php endif; ?>
<?php /************************
	block clients
************************/?>
<?php if( get_row_layout() == 'block_clients' ): ?>
<?php 
$navigation = get_sub_field('navigation');
$pagination = get_sub_field('pagination');
$time = get_sub_field('time');
$autoplay = get_sub_field('autoplay');
$column = get_sub_field('column');
?>
<script type="text/javascript">
	jQuery(document).ready(function ($) {
		/************************
			Slide
			************************/
		var mySwiper = new Swiper('#block_clients', {
			<?php // Optional parameters ?> 
			<?php //pagination ?>
			<?php if ($pagination) : ?>
			pagination: '.swiper-pagination',
			paginationClickable: true,
			<?php endif; ?> 
			<?php if ($navigation) : ?>
			nextButton: '.swiper-button-next',
			prevButton: '.swiper-button-prev',
			<?php endif; ?>
			slidesPerView: <?php echo	esc_html($column); ?>,
			<?php //time ?>
			<?php if ($autoplay =="true") : ?>
			autoplayDisableOnInteraction: false,
			autoplay: <?php echo esc_html($time); ?>,
			<?php endif; ?> 
			spaceBetween: 0,
			preloadImages: true,
			breakpoints: {
			640: {
			slidesPerView: 1,
			spaceBetween: 20
		},320: {
			slidesPerView: 1,
			spaceBetween: 10
			}
		}
		}); 
	});
</script>
<?php endif; ?>
<?php /************************
	block clients
************************/ ?>
<?php if( get_row_layout() == 'block_carousels' ): ?>
<?php 
$navigation = get_sub_field('navigation');
$pagination = get_sub_field('pagination');
$time = get_sub_field('time');
$autoplay = get_sub_field('autoplay');
$column = get_sub_field('column');?>
<script type="text/javascript">
	jQuery(document).ready(function ($) {
		/************************
			Slide
			************************/
		var mySwiper = new Swiper('#block_carousel<?php echo get_row_index(); ?>', {
			<?php // Optional parameters ?> 
			<?php //pagination ?>
			<?php if ($pagination) : ?>
			pagination: '.swiper-pagination',
			paginationClickable: true,
			<?php endif; ?> 
			<?php if ($navigation) : ?>
			nextButton: '.swiper-button-next',
			prevButton: '.swiper-button-prev',
			<?php endif; ?>
			slidesPerView: <?php echo	esc_html($column); ?>,
			<?php //time ?>
			<?php if ($autoplay =="true") : ?>
			autoplayDisableOnInteraction: false,
			autoplay: <?php echo esc_html($time); ?>,
			<?php endif; ?> 
			spaceBetween: 0,
			preloadImages: true,
			breakpoints: {
			640: {
			slidesPerView: 1,
			spaceBetween: 20
		},
		320: {
		slidesPerView: 1,
		spaceBetween: 10,
		}
		}
		}); 
	});
</script>
<?php endif; ?>
<?php /************************
	block	portfolio
************************/?>
<?php if( get_row_layout() == 'block_masonry' ): ?>
<script type="text/javascript">
	/************************
			Masonry
			***********************/
/*
	jQuery(document).ready(function ($) {
		var masonryInit = true; // set Masonry init flag
		$.fn.almComplete = function(alm){ // Ajax Load More callback function
			if($('#ajax-load-more').length){
				var $container = $('#ajax-load-more .alm-listing '); // our container
				if(masonryInit){
					// initialize Masonry only once
					masonryInit = false;
					$container.masonry({ 
						itemSelector: '.grid-item',
						percentPosition: true,
						layoutMode: 'masonry',
					});		      
				}else{
					$container.masonry('reloadItems'); // Reload masonry items after callback
				}
				$container.imagesLoaded( function() { // When images are loaded, fire masonry again.
					$container.masonry();
				});
			}
		};
   
   var masonryInit = true; // set Masonry init flag
		$.fn.almComplete = function(alm){ // Ajax Load More callback function
			if($('#ajax-load-more-2').length){
				var $container = $('#ajax-load-more-2 .alm-listing '); // our container
				if(masonryInit){
					// initialize Masonry only once
					masonryInit = false;
					$container.masonry({ 
						itemSelector: '.grid-item',
						percentPosition: true,
						layoutMode: 'masonry',
					});		      
				}else{
					$container.masonry('reloadItems'); // Reload masonry items after callback
				}
				$container.imagesLoaded( function() { // When images are loaded, fire masonry again.
					$container.masonry();
				});
			}
		};
		//filter
		var alm_is_animating = false; // Animating flag
		$('.alm-filter-nav button').eq(0).addClass('btn-readmore'); // Set initial active state
		// Btn Click Event
		$('.alm-filter-nav button').on('click', function(e){    
			e.preventDefault();  
			var el = $(this); // Our selected element     

			if(!el.hasClass('btn-readmore') && !alm_is_animating){ // Check for active and !alm_is_animating  
				alm_is_animating = true;   
				el.addClass('btn-readmore').siblings('button').removeClass('btn-readmore'); // Add active state

				var data = el.data(), // Get data values from selected menu item
								transition = 'masonry', // 'slide' | 'fade' | null
								speed = '300'; //in milliseconds

				$.fn.almFilter(transition, speed, data); // Run the filter     
			}      
		});
		$.fn.almFilterComplete = function(){      
			alm_is_animating = false; // clear animating flag
		};
	});
  */
</script>
<?php endif; ?>
<?php /************************
	block	tabs
************************/?>
<?php if( get_row_layout() == 'block_tabs' ): ?>
<script type="text/javascript">
	jQuery(document).ready(function ($) {
		/************************
				Tabs
				************************/
		$('.nav-tabs > li > a').click(function (event) {
			event.preventDefault();
			$(this).tab('show');
		});
		$('.nav-tabs-line > li > a').click(function (event) {
			event.preventDefault();
			$(this).tab('show');
		});
	});
</script>
<?php endif; ?>
<?php /************************
	block	numbers
************************/?>
<?php if( get_row_layout() == 'block_numbers' ): ?>
<script type="text/javascript">
	jQuery(document).ready(function ($) {
		//animate number
		jQuery('.content-number-block .animate-count-block').addClass("hidden-scroll").viewportChecker({
			callbackFunction: function(elem, action) {
				$('.content-number-block .timer').countTo();
			}
		});
		jQuery('#content-number<?php echo get_row_index(); ?> .animate-count').addClass("hidden-scroll").viewportChecker({
			callbackFunction: function(elem, action) {
				$('#content-number<?php echo get_row_index(); ?> .timer').countTo();
			}
		});
	});	
</script>
<?php endif; ?>
<?php /************************
	block	skills
************************/?>
<?php if( get_row_layout() == 'block_skills' ): ?>
<script type="text/javascript">
	//***************************************************
	jQuery(document).ready(function($) {
		//  animate progress bar
		$.fn.animateBar<?php echo get_row_index(); ?> = function() {
			$('#franky-skills<?php echo get_row_index(); ?> .progress-bar').each(function() {
				$(this).css('width', $(this).attr('aria-valuenow') + '%');
			});
		};
		jQuery('#franky-skills<?php echo get_row_index(); ?>').viewportChecker({
			offset: 100,
			callbackFunction: function(elem, action) {
				jQuery('.progress').animateBar<?php echo get_row_index(); ?>();
			}
		});
	});
</script>
<?php endif; ?>
<?php /************************
	call to action
************************/?>
<?php if( get_row_layout() == 'call_to_action' ): 
$select_call_to_action = get_sub_field('select_call_to_action');
$post = $select_call_to_action;
setup_postdata( $post );
?>
<?php 
$rowN = get_row_index();	
$video_id = get_field('video_url');
?>
<?php if ($video_id) : ?>
<script type="text/javascript">
	
	
	jQuery(document).ready(function($) {
		var containerW = jQuery('.call-to-action #iframe_<?php echo esc_html($rowN); ?>').width();
		var containerH = jQuery('.call-to-action #iframe_<?php echo esc_html($rowN); ?>').height();
		function iframeWidth(containerW,containerH) {
			$('#iframe_<?php echo esc_html($rowN); ?> iframe').width(containerW);
			// iframe size width:640 height:360
			var r = 640/360;
			$('#iframe_<?php echo esc_html($rowN); ?> iframe').height(containerW / r);
		}
		iframeWidth(containerW,containerH);
		function callResize () {
			$(window).resize(function(){
				var containerW = $('.call-to-action #iframe_<?php echo esc_html($rowN); ?>').width();
				var containerH = $('.call-to-action #iframe_<?php echo esc_html($rowN); ?>').height();
				iframeWidth(containerW,containerH);
			});
		}
});		
</script>
<?php endif; ?>
<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>
<?php /************************
	block	map
************************/?>
<?php if( get_row_layout() == 'block_map' ): ?>
<?php //
//vars
$maps = get_sub_field('locations'); ?>
<!--Block contact-->
<?php if( !empty($maps) ):?>
<?php $google_key  = get_theme_mod('franky_map_google_key'); ?>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo esc_html($google_key); ?>"></script>
<script type="text/javascript">
	(function($) {
		/*
*  new_map
*  This function will render a Google Map onto the selected jQuery element
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$el (jQuery element)
*  @return	n/a
*/
		/*style map*/
		<?php 
		$custom_marker = get_sub_field('custom_marker');
		$styleMap = get_sub_field('style_map');
		if (empty($styleMap)) { ?>
		var styleMap = '[{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]';
		<?php } else { ?>
		var styleMap = <?php the_sub_field('style_map'); ?>;
		<?php } ?>
		function new_map( $el ) {
			// var
			var $markers = $el.find('.marker');
			// vars
			var args = {
				center		: new google.maps.LatLng(0, 0),
				mapTypeId	: google.maps.MapTypeId.ROADMAP,
				styles: styleMap,
				scrollwheel: false,
			};

			// create map	        	
			var map = new google.maps.Map( $el[0], args);

			// add a markers reference
			map.markers = [];

			// add markers
			$markers.each(function(){
				add_marker( $(this), map );
			});
			// center map
			center_map( map );
			// return
			return map;
		}
		/*
*  add_marker
*
*  This function will add a marker to the selected Google Map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$marker (jQuery element)
*  @param	map (Google Map object)
*  @return	n/a
*/
		function add_marker( $marker, map ) {
			// var
			var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );
			// create marker
			var marker = new google.maps.Marker({
				position	: latlng,
				map			: map,
				icon : '<?php echo $custom_marker; ?>',

			});
			// add to array
			map.markers.push( marker );
			// if marker contains HTML, add it to an infoWindow
			if( $marker.html() )
			{
				// create info window
				var infowindow = new google.maps.InfoWindow({
					content		: $marker.html()
				});
				// show info window when marker is clicked
				google.maps.event.addListener(marker, 'click', function() {
					infowindow.open( map, marker );
				});
			}
		}
		/*
*  center_map
*  This function will center the map, showing all markers attached to this map
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	map (Google Map object)
*  @return	n/a
*/
		function center_map( map ) {
			// vars
			var bounds = new google.maps.LatLngBounds();
			// loop through all markers and create bounds
			$.each( map.markers, function( i, marker ){
				var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
				bounds.extend( latlng );
			});
			// only 1 marker?
			if( map.markers.length == 1 )
			{
				// set center of map
				map.setCenter( bounds.getCenter() );
				map.setZoom( 16 );
			}
			else
			{
				// fit to bounds
				map.fitBounds( bounds );
			}
		}
		/*
*  document ready
*  This function will render each map when the document is ready (page has loaded)
*  @type	function
*  @date	8/11/2013
*  @since	5.0.0
*  @param	n/a
*  @return	n/a
*/
		// global var
		var map = null;
		$(document).ready(function(){
			$('.acf-map').each(function(){
				// create map
				map = new_map( $(this) );
			});
		});
	})(jQuery);
</script>
<?php endif; ?>
<!--End Block map-->
<?php endif; ?>
<?php endwhile; ?>
<?php endif; //end 	flexi content
}
?>