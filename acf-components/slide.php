<?php
/////////////////////////
// SLIDE PAGE
////////////////////////
?>
<?php if( have_rows('slider')): ?>
<?php 
$heightSlide = get_field('max_height');
$container = get_field('container');
$background_color_container = get_field('background_color_container');
$background_color_container = ($background_color_container) ? $background_color_container : '#ffffff';
$units = get_field('units');
$maxHeight = ($heightSlide)? 'max-height:'. $heightSlide . $units . ';' : 'max-height:100vh;';?>
<!--Slide header-->
<div id="content-slide" style="background-color:<?php echo esc_html($background_color_container); ?>">
	<div class="slide-header">
		<?php if ($container == 'container') { ?>
		<div class="container">
			<div class="row">
				<div class="col-md-12">	
					<?php } ?>
					<div class="swiper-container swiper-container-slide" style="<?php echo esc_html($maxHeight); ?>">
						<div class="swiper-wrapper">
							<?php while( have_rows('slider') ): the_row(); 
							// vars
							$bgcolor = get_sub_field('background_color');
							$background = ($bgcolor)? 'background-color:'. $bgcolor . ';' : 'background-color:#ccc;';
							$color = get_sub_field('color');
							$color = ($color)? 'color:'. $color . ';' : 'color:#fff;';
							$image = get_sub_field('image');
							$caption_title = get_sub_field('caption_title');
							$caption_italic = get_sub_field('caption_italic');
							$caption_description = get_sub_field('caption_description');
							$navigation = get_field('navigation');
							$align = get_sub_field('align_text');
							$bg_overlay = get_sub_field('background_overlay');
							$bg_overlay_color = ($bg_overlay)? 'background-color:'. $bg_overlay . ';' : 'background-color:transparent;';
							$opacity_overlay = get_sub_field('overlay_opacity');
							$opacity_overlay_color = ($opacity_overlay)? 'opacity:'. $opacity_overlay . ';' : 'opacity:0.4;';		
							//animations
							$animation_caption_italic = get_sub_field('animation_caption_italic');
							$animation_caption_italic = ($animation_caption_italic) ?  $animation_caption_italic : 'fadeIn';
							$animation_title = get_sub_field('animation_title');
							$animation_title = ($animation_title) ?  $animation_title : 'fadeIn';
							$animation_description = get_sub_field('animation_description');
							$animation_description = ($animation_description) ?  $animation_description : 'fadeIn';
							$animation_button = get_sub_field('animation_button');
							$animation_button = ($animation_button) ?  $animation_button : 'fadeIn';
							//delay
							$delay_caption_italic = get_sub_field('delay_caption_italic');
							$delay_caption_italic = ($delay_caption_italic) ?  $delay_caption_italic : 'delay-2s';
							$delay_title = get_sub_field('delay_title');
							$delay_title = ($delay_title) ?  $delay_title : 'delay-2s';
							$delay_description = get_sub_field('delay_description');
							$delay_description = ($delay_description) ?  $delay_description : 'delay-2s';
							$delay_button = get_sub_field('delay_button');
							$delay_button = ($delay_button) ?  $delay_button : 'delay-2s';
							//speed
							$speed_time_italic = get_sub_field('speed_time_italic');
							$speed_time_italic = ($speed_time_italic) ?  $speed_time_italic : 'slower';
							$speed_time_title = get_sub_field('speed_time_title');
							$speed_time_title = ($speed_time_title) ?  $speed_time_title : 'slower';
							$speed_time_description = get_sub_field('speed_time_description');
							$speed_time_description = ($speed_time_description) ?  $speed_time_description : 'slower';
							$speed_time_button = get_sub_field('speed_time_button');
							$speed_time_button = ($speed_time_button) ?  $speed_time_button : 'slower';



							if ($image['url']) {
								$imageUrl = $image['url'];
								$styleSlide = 'background-image:url('. $imageUrl .'); '. $background .';';
							}
							else {
								$styleSlide = $background .';';
							}


							?>
							<div class="swiper-slide slide-parallax <?php echo esc_html($align); ?>" <?php  echo 'style="' . esc_html($styleSlide) . '"'; ?>>
								<div class="overlay" style="<?php echo esc_html($bg_overlay_color); ?> <?php echo esc_html($opacity_overlay_color); ?>"></div>
								<div class="caption-tex">
									<div data-enllax-ratio="-0.1" data-enllax-type="foreground">
										<?php if( $caption_italic): ?>
										<div class="italic-slide animated	<?php echo esc_html($animation_caption_italic); echo esc_html(' '. $delay_caption_italic); echo esc_html(' '. $speed_time_italic); ?>" style="<?php echo esc_html($color); ?>">
											<?php the_sub_field('caption_italic'); ?>
										</div>
										<?php endif; ?>
										<?php if( $caption_title): ?>
										<div class="title-slide animated <?php echo esc_html($animation_title); echo esc_html(' '. $delay_title); echo esc_html(' '. $speed_time_title); ?>" style="<?php echo esc_html($color); ?>">
											<?php the_sub_field('caption_title'); ?>	
										</div>
										<?php endif; ?>
										<div>
											<?php if( $caption_description): ?>
											<div class="text-slide animated <?php echo esc_html($animation_description); echo esc_html(' '. $delay_description);  echo esc_html(' '. $speed_time_description);?>" style="<?php echo esc_html($color); ?>">
												<?php the_sub_field('caption_description'); ?>	
											</div>
											<?php endif; ?>
										</div>
										<?php if( have_rows('buttons')): ?>
										<div class="readmore-slide animated <?php echo esc_html($animation_button); echo esc_html(' '. $delay_button); echo esc_html(' '. $speed_time_button); ?>" >
											<?php while ( have_rows('buttons') ) : the_row(); 
											//var 
											$button_link = get_sub_field('button_link');
											$button_text = get_sub_field('button_text');	
											$button_class = get_sub_field('button_class');
											$button_class	= ($button_class) ? $button_class : 'btn-readmore';
											?>
											<a href="<?php echo esc_html($button_link); ?>" class="btn <?php echo esc_html($button_class); ?>"><?php echo esc_html($button_text); ?></a>
											<?php endwhile; ?>
										</div>
										<?php endif; ?>
									</div>
								</div>
								<div class="preloader"></div>
							</div>
							<?php endwhile; ?>
						</div>
						<!-- Add Pagination -->
						<div class="swiper-pagination"></div>
						<?php if ($navigation) : ?>
						<!-- Add Arrows -->
						<div class="swiper-button-next swiper-button-white hidden-xs"></div>
						<div class="swiper-button-prev swiper-button-white hidden-xs"></div>
						<?php endif; ?>
					</div>
					<?php if ($container == 'container') { ?>
				</div>
			</div>
		</div>
		<?php } ?>
	</div><!--end Slide header-->
</div><!--end content-slide-->
<?php endif; ?> 