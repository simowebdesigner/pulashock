<?php if( get_row_layout() == 'block_skills' ): ?>
<?php 
// vars
$background_color = get_sub_field('background_color');
$background_color = ($background_color)? $background_color:'transparent';
$paddingTop = get_sub_field('padding_top');
$paddingBottom = get_sub_field('padding_bottom');
$paddingTop  = (!empty($paddingTop) || ($paddingTop == '0')) ? 'padding-top:' . $paddingTop . 'px;' : $paddingTop = 'padding-top:80px;';
$paddingBottom  = (!empty($paddingBottom) || ($paddingBottom == '0')) ? 'padding-bottom:' .  $paddingBottom . 'px;' : $paddingBottom = 'padding-bottom:80px;';
$align_text = get_sub_field('align_text_side');
$enable_text_side = get_sub_field('enable_text_side');
$class = ($enable_text_side)? 'col-lg-6' : 'col-lg-12';
$text_side = get_sub_field('text_side');
$layout = get_sub_field('layout');
$layout = ($layout )? $layout  : 'default';
$button_text = get_sub_field('button_text');
$button_link = get_sub_field('button_link');
$id = get_sub_field('id');
?>
<section <?php if($id): ?>id="<?php echo esc_html($id); ?>"<?php endif; ?> class="block-skills block-flex animate-bar" style="<?php echo esc_html($paddingTop); ?> <?php echo esc_html($paddingBottom); ?> background-color:<?php echo esc_html($background_color); ?>;">
	<div class="container">
		<div class="row">
			<?php if($enable_text_side) : ?>
			<?php if($align_text == "left") : ?>
			<div class="col-lg-6">
				<?php the_sub_field('text_side'); ?>
				<?php if ($button_text): ?>
				<div class="clear">
					<a href="<?php echo esc_html($button_link); ?>" class="btn btn-readmore mt-20 mb-20"><?php echo esc_html($button_text); ?></a>
				</div>
				<?php endif; ?>
			</div>
			<?php endif; ?>
			<?php endif; ?>
			<div class="<?php echo esc_html($class); ?>" id="franky-skills<?php echo get_row_index(); ?>">
				<?php if (have_rows('skill')) : ?>
				<?php if ($layout == 'default') : ?>
				<?php while( have_rows('skill') ): the_row(); 
				// vars
				$title = get_sub_field('title');
				$percentage = get_sub_field('percentage');
				$color = get_sub_field('color');
				?>
				<?php if ($title) : ?>
				<h4><?php echo esc_html($title); ?></h4>
				<?php endif; ?>
				<div class="progress">
					<div class="progress-bar" role="progressbar" aria-valuenow="<?php echo esc_html($percentage); ?>"
							 aria-valuemin="0" aria-valuemax="100" style="width:0%; background-color:<?php echo esc_html($color . ';'); ?>">
						<?php echo esc_html($percentage . '%'); ?>
					</div>
					<div class="clear"></div>
				</div>
				<?php endwhile; ?>
				<?php elseif ($layout == 'chart') : ?>
				    <div id="donutchart" style="width: 100%;height:500px;"></div>
				<?php endif; ?>
				
				<?php endif; ?>
			</div>
			<?php if($enable_text_side) : ?>
			<?php if($align_text == "right") : ?>
			<div class="col-lg-6">
				<?php the_sub_field('text_side'); ?> 
				<?php if ($button_text): ?>
				<div class="clear">
					<a href="<?php echo esc_html($button_link); ?>" class="btn btn-readmore mt-20 mb-20"><?php echo esc_html($button_text); ?></a>
				</div>
				<?php endif; ?>
			</div>
			<?php endif; ?>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php endif; ?>