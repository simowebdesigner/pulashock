
<?php if( get_row_layout() == 'block_news' ): ?>
<?php 
//include generic style
include(locate_template('acf-components/include/style_block.php')); 
$exclude_article = get_sub_field('exclude_article');
$title_article = get_sub_field('title_article');
$title_article = ($title_article) ? $title_article : 'h3';
$columns = get_sub_field('columns');
$columns = ($columns) ? $columns : 'col-md-4';
$order_post = get_sub_field('order_post');
$order_by = get_sub_field('order_by');
?>
<!--Block news-->
<section class="block-news block-flex <?php echo $class; ?>" <?php echo $id, $parallax_data;?>>
	<div class="container">
		<div class="row">
			<?php // vars
			$cateId = get_sub_field('latest_news'); 
			$numberPost = get_sub_field('number'); 
			?>
			<?php 
			$args = array(
				'post_type' => 'post',
				'orderby' => $order_by,
				'order' => $order_post,
				'posts_per_page' => $numberPost,
				'cat' => $cateId,
				 'post__not_in' => $exclude_article,
			);
			?>
			<?php $query = new WP_Query( $args ); ?>
			<?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
			<div class="<?php echo esc_html($columns); ?> fade-scroll" data-vp-add-class="fadeIn animated">
				<article <?php post_class(); ?>>
					<div class="content-post" data-vp-add-class="fadeIn animated" >
						<?php if ( has_post_thumbnail()): ?> 
						<figure>
							<a class="link-post" href="<?php the_permalink(); ?>">
								<div class="overlay"></div>
								<?php echo get_the_post_thumbnail($post->ID, 'news-thumb', array('class' => 'img-responsive w_full')); ?>
							</a>	
						</figure>
						<?php endif; ?>
						<div class="box-text">
							<header>
								<a href="<?php the_permalink(); ?>"><<?php echo esc_html($title_article); ?> class="entry-title"><?php the_title(); ?></<?php echo esc_html($title_article); ?>></a>
								<?php get_template_part('templates/entry-meta'); ?>
							</header>
							<div class="entry-summary">
								<?php the_excerpt(); ?>
							</div>
							<?php echo '<a href="'.get_permalink().'" class="btn btn-readmore mt-20">'.__('Read more', 'franky').'</a>'; ?>
						</div>
					</div>
				</article>
			</div><!--end col-md-->
			<?php endwhile; ?>
		</div><!--end row-->
		<?php else: ?>
		<div class="alert alert-warning">
			<?php _e('Sorry, no results were found.', 'franky'); ?>
		</div>
		<?php  endif; ?>

		<?php wp_reset_query(); ?>
	</div><!--end container-->
</section>
<!--End Block news-->
<?php endif; ?>