<?php if( get_row_layout() == 'block_alternate' ): ?>
<!--Block alternate-->
<?php  // vars
$image = get_sub_field('image');
$button = get_sub_field('button_link');
$button_link = get_sub_field('button_link');
$title = get_sub_field('title');
$insert_image = get_sub_field('insert_image');
$container = get_sub_field('container');
$container = ($container) ? $container : 'container';
$text = get_sub_field('text');
$align_image = get_sub_field('align_image');
$image_width = get_sub_field('image_width');
//parallax
$parallax = get_sub_field('enable_parallax');
$parallax_data = get_sub_field('data_parallax');
$parallax_data = ($parallax)? 'data-enllax-ratio = " ' . $parallax_data . ' "' :'';
$parallax_class = ($parallax)? 'parallax':'no_parallax';
//attribute
$class = get_sub_field('class');
$class = ($class) ? ' ' . $class : '';
$class = $class . ' ' . $parallax_class; 
//bg
$style_bg = get_sub_field('background');
//overlay
$background_overlay = get_sub_field('background_overlay');
$overlay_opacity = get_sub_field('overlay_opacity');
if ($style_bg == 'image'):
$overlayDiv = '<div class="parallax-overlay"></div>';
else :
$overlayDiv = '';
endif;
?>
<section class="block-alternate block-flex <?php echo $class; ?>" id="flexible-section-<?php echo get_row_index(); ?>" <?php echo $parallax_data;?>>
	<?php // align image right  ?>
	<?php if ($align_image == "image-right"): ?>
	<?php
	//col text
	if ($image_width == 'col-md-4') {
		$col_text = "col-md-8";
	}
	elseif ($image_width == 'col-md-6') {
		$col_text = "col-md-6";
	}
	elseif ($image_width == 'col-md-8') {
		$col_text = "col-md-4";
	}
	?>
	<div class="block-flex">
	<?php echo $overlayDiv; ?>
		<div class="<?php echo esc_html($container); ?>">
			<div class="row">
				<div class="<?php echo esc_html($col_text); ?>">
					<div class="content-text">
						<?php if ($title): ?> 
						<h2 data-vp-add-class="fadeInLeft animated" class="fade-scroll"><?php echo esc_html($title); ?></h2>
						<?php endif; ?>
						<?php if ($text): ?> 
						<div class="box-text fade-scroll" data-vp-add-class="fadeInLeft animated">
							<?php the_sub_field('text'); ?> 
						</div>
						<?php endif; ?>
						<?php if ($button): ?> 
						<div class="box-button">
						<a href="<?php echo esc_html($button['url']); ?>" title="<?php echo esc_html($button['title']); ?>" target="<?php echo esc_html($button['target']); ?>" class="btn btn-readmore btn-lg mt-20 mb-20 fade-scroll" data-vp-add-class="fadeInLeft animated"><?php echo $button['title']; ?></a>
						</div>
						<?php endif; ?>
					</div>
				</div><!--end col-md-8-->
				<?php if( !empty($image) ): ?>
				<div class="<?php echo esc_html($image_width); ?>">
					<?php if ($insert_image == 'background') : ?>
					<div class="box-img fade-scroll" data-vp-add-class="fadeInRight animated" style="background-image: url(<?php echo esc_html($image['url']); ?>);">
					</div>
					<?php else : ?>
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"  class="img-responsive"/>
					<?php endif; ?>
				</div>
				<?php endif; ?>
			</div><!--end row-->
		</div><!--end container-->
	</div><!--end alternate-bg1-->
	<?php // align image left  ?>
	<?php else: ?>
	<?php
	//col text
	if ($image_width == 'col-md-4') {
		$col_text = "col-md-8 push-md-4";
		$col_image ="col-md-4 pull-md-8";
	}
	elseif ($image_width == 'col-md-6') {
		$col_text = "col-md-6 push-md-6";
		$col_image ="col-md-6 pull-md-6";
	}
	elseif ($image_width == 'col-md-8') {
		$col_text = "col-md-4 push-md-8";
		$col_image ="col-md-8 pull-md-4";
	}
	?>
	<div class="block-flex lazy" >
		<?php if ($style_bg == 'image') : ?><div class="parallax-overlay" style="background-color:<?php echo esc_html($background_overlay); ?>; opacity:<?php echo esc_html($overlay_opacity); ?>"></div><?php endif;?>
		<div class="<?php echo esc_html($container); ?>">
			<div class="row">
				<div class="<?php echo esc_html($col_text); ?>">
					<div class="content-text">
						<?php if ($title): ?> 
						<h2 class="fade-scroll" data-vp-add-class="fadeInRight animated"><?php echo esc_html($title); ?></h2>
						<?php endif; ?>
						<?php if ($text): ?> 
						<div class="box-text fade-scroll" data-vp-add-class="fadeInRight animated">
							<?php the_sub_field('text'); ?>
						</div>
						<?php endif; ?>
						<?php if ($button): ?> 
						<div class="box-button">
						<a href="<?php echo esc_html($button['url']); ?>" title="<?php echo esc_html($button['title']); ?>" target="<?php echo esc_html($button['target']); ?>" class="btn btn-readmore btn-lg mt-20 mb-20 fade-scroll" data-vp-add-class="fadeInLeft animated"><?php echo $button['title']; ?></a>
						</div>
						<?php endif; ?>
					</div>
				</div><!--end col-md-8-->
				<?php if( !empty($image) ): ?> 
				<div class="<?php echo esc_html($col_image); ?>">
					<?php if ($insert_image == 'background') : ?>
					<div class="box-img fade-scroll" data-vp-add-class="fadeInLeft animated" style="background-image: url(<?php echo esc_html($image['url']); ?>);">
					</div>
					<?php else : ?>
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"  class="img-responsive"/>
					<?php endif; ?>
				</div>
				<?php endif; ?>
			</div><!--end row-->
		</div><!--end container-->
	</div><!--end alternate-bg2-->
	<?php endif; ?>
</section>
<!--End Block alternate-->
<?php endif; ?>