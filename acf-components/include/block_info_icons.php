<?php if( get_row_layout() == 'block_info_icons' ): ?>
<?php
// vars
$background_image = get_sub_field('background_image');
$background_overlay = get_sub_field('background_overlay');
$background_overlay = ($background_overlay)? $background_overlay:'transparent';
$background_color = get_sub_field('background_color_box');
$background_color = ($background_color)? $background_color:'transparent';
$background_color_icon = get_sub_field('background_color');
$background_color_icon = ($background_color_icon)? $background_color_icon:'transparent';
$overlay_opacity = get_sub_field('overlay_opacity');
$color_text = get_sub_field('color_text');
$color = ($color_text)? $color_text:'';
$image = get_sub_field('image');
$parallax = get_sub_field('enable_parallax');
$parallax_data = get_sub_field('data_parallax');
$parallax_data = ($parallax)? 'data-enllax-ratio = " ' . $parallax_data . ' "' :'';
$parallax_class = ($parallax)? 'parallax':'no_parallax';
$paddingTop = get_sub_field('padding_top');
$paddingBottom = get_sub_field('padding_bottom');
$paddingTop  = (!empty($paddingTop) || ($paddingTop == '0')) ? 'padding-top:' . $paddingTop . 'px;' : $paddingTop = 'padding-top:40px;';
$paddingBottom  = (!empty($paddingBottom) || ($paddingBottom == '0')) ? 'padding-bottom:' .  $paddingBottom . 'px;' : $paddingBottom = 'padding-bottom:40px;';
$image = get_sub_field('image_central_block_info_icons');
$color_box = get_sub_field('color_text_box');
$color = get_sub_field('color_text');
$button_text = get_sub_field('button_text');
$button_link = get_sub_field('button_link');
$style_bg = get_sub_field('background');
$id = get_sub_field('id');
?>
<!--block info box-->
<section <?php if($id): ?>id="<?php echo esc_html($id); ?>"<?php endif; ?> class="block-info-box block-flex lazy <?php echo esc_html($parallax_class); ?>" <?php if ($style_bg == 'image') : ?>data-src="<?php echo esc_html($background_image['url']); ?>"<?php endif; ?> style="<?php echo esc_html($paddingTop); ?> <?php echo esc_html($paddingBottom); ?>; background-color:<?php echo esc_html($background_color); ?>" <?php if ($style_bg == 'image') : ?><?php echo esc_html($parallax_data); ?><?php endif; ?> >
	<?php if ($style_bg == 'image') : ?><div class="parallax-overlay" style="background-color:<?php echo esc_html($background_overlay); ?>; opacity:<?php echo esc_html($overlay_opacity); ?>"></div><?php endif; ?>
	<div class="container">
			<div class="featuers-wrapper row">
				<!-- CLOSE IMAGE -->
				<div class="features-center fade-scroll col-lg-4 push-lg-4 text-center" data-vp-add-class="fadeIn animated">
					<?php if( !empty($image) ): ?>
					<?php echo wp_get_attachment_image(get_sub_field('image_central_block_info_icons'), 'full', 0, array('class' => 'img-responsive')); ?>
					<?php endif; ?>
				</div>
				<?php if( have_rows('left_icons_block_info_icons') ): ?>
				<!-- FEATURES LEFT -->
				<div class="col-lg-4 pull-lg-4">
					<?php while( have_rows('left_icons_block_info_icons') ): the_row(); 
// vars
$icon = get_sub_field('icon');
$title = get_sub_field('title');
$text = get_sub_field('text'); 
					?>
					<?php if ($icon): ?>
					<div class="features-left fade-scroll" data-vp-add-class="fadeInLeft animated">
						<div class="block-icon" style="color:<?php echo esc_html($color); ?>; background-color:<?php echo esc_html($background_color_icon); ?>">
							<?php the_sub_field('icon'); ?>
						</div>
						<div class="block-text" style="color:<?php echo esc_html($color_box); ?>">
							<?php if ($title): ?>
							<h5 style="color:<?php echo esc_html($color_box); ?>"><?php echo esc_html($title); ?></h5>
							<?php endif; ?>
							<?php if ($text): ?>
							<?php the_sub_field('text'); ?>
							<?php endif; ?>
						</div>
					</div>
					<?php endif; ?>
					<?php endwhile; ?>
				</div><!--end col-md-4 -->
				<?php endif; ?>
				<?php if( have_rows('right_icons_block_info_icons') ): ?>
				<!-- FEATURES RIGHT -->
				<div class="col-lg-4">
					<?php while( have_rows('right_icons_block_info_icons') ): the_row(); 
// vars
$icon = get_sub_field('icon');
$title = get_sub_field('title');
$text = get_sub_field('text'); 
					?>
					<?php if ($icon): ?>
					<div class="features-right fade-scroll" data-vp-add-class="fadeInRight animated">
						<div class="block-icon" style="color:<?php echo esc_html($color); ?>;background-color:<?php echo esc_html($background_color_icon); ?>;">
							<?php the_sub_field('icon'); ?>
						</div>
						<div class="block-text" style="color:<?php echo esc_html($color_box); ?>">
							<?php if ($title): ?>
							<h5 style="color:<?php echo esc_html($color_box); ?>"><?php echo esc_html($title); ?></h5>
							<?php endif; ?>
							<?php if ($text): ?>
							<?php the_sub_field('text'); ?>
							<?php endif; ?>
						</div>
					</div>
					<?php endif; ?>
					<?php endwhile; ?>
				</div><!--end col-md-4 -->
				<?php endif; ?>
			</div><!--end featuers-wrapper-->
	</div><!--end container-->
</section>
<!--end block info box--> 
<?php endif; ?>