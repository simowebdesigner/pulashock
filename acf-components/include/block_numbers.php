<?php if( get_row_layout() == 'block_numbers' ): ?>
<!--block numbers-->
<?php 
// vars
$background_image = get_sub_field('background_image');
$background_overlay = get_sub_field('background_overlay');
$background_overlay = ($background_overlay)? $background_overlay:'transparent';
$background_color = get_sub_field('background_color');
$background_color = ($background_color)? $background_color:'transparent';
$overlay_opacity = get_sub_field('overlay_opacity');
$color_text = get_sub_field('color_text');
$color = ($color_text)? $color_text:'';
$image = get_sub_field('image');
$parallax = get_sub_field('enable_parallax');
$parallax_data = get_sub_field('data_parallax');
$parallax_data = ($parallax)? 'data-enllax-ratio = '. $parallax_data .'' :'';
$parallax_class = ($parallax)? 'parallax':'no_parallax';
$paddingTop = get_sub_field('padding_top');
$paddingBottom = get_sub_field('padding_bottom');
$paddingTop  = (!empty($paddingTop) || ($paddingTop == '0')) ? 'padding-top:' . $paddingTop . 'px;' : $paddingTop = 'padding-top:40px;';
$paddingBottom  = (!empty($paddingBottom) || ($paddingBottom == '0')) ? 'padding-bottom:' .  $paddingBottom . 'px;' : $paddingBottom = 'padding-bottom:40px;';
$style_bg = get_sub_field('background');
$id = get_sub_field('id');
$rowN = get_row_index();
?>
<section <?php if($id): ?>id="<?php echo esc_html($id); ?>"<?php endif; ?> class="block-numbers block-flex block-columns lazy <?php echo esc_html($parallax_class); ?>" <?php if ($style_bg == 'image') : ?>data-src="<?php echo esc_html($background_image['url']); ?>"<?php endif; ?> style="<?php echo esc_html($paddingTop); ?> <?php echo esc_html($paddingBottom); ?> color:<?php echo esc_html($color); ?>; background-color:<?php echo esc_html($background_color); ?>;" <?php echo esc_html($parallax_data); ?>>
	<?php if ($style_bg == 'image') : ?><div class="parallax-overlay" style="background-color:<?php echo esc_html($background_overlay); ?>; opacity:<?php echo esc_html($overlay_opacity); ?>;"></div><?php endif; ?>
	<div class="container container-number-text">
		<div class="row row-no-margin text-center" id="content-number<?php echo esc_html($rowN); ?>">
			<?php while( have_rows('count_number') ): the_row(); 
// vars
$icon = get_sub_field('icon');
$title = get_sub_field('title');
$text = get_sub_field('text');
$from_number = get_sub_field('from_number'); 
$to_number = get_sub_field('to_number'); 
$speed_time = get_sub_field('speed_time'); 
			?>
			<div data-vp-add-class="fadeIn animated" class="col animate-count hidden-scroll fadeIn animated">
				<?php if ($icon) : ?>
				<div class="block-icon mb-10">
					<?php the_sub_field('icon'); ?>
				</div>
				<?php endif; ?>
				<?php if ($title) : ?>
				<h4 style="color:<?php echo esc_html($color); ?>"><?php echo esc_html($title); ?></h4>
				<?php endif; ?>
				<div data-speed="<?php echo esc_html($speed_time); ?>" data-from="<?php echo esc_html($from_number); ?>" data-to="<?php echo esc_html($to_number) ?>" class="timer"><?php echo esc_html($to_number) ?></div>
			</div>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>