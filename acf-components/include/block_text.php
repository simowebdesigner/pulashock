<?php if( get_row_layout() == 'block_text' ): ?>
<!--block text-->
<?php 
//vars
$title = get_sub_field('title');
$text = get_sub_field('text');
$image = get_sub_field('image');
$editor = get_sub_field('editor'); 
$container = get_sub_field('container');
$container = ($container) ? $container : 'container';

//parallax
$parallax = get_sub_field('enable_parallax');
$parallax_data = get_sub_field('data_parallax');
$parallax_data = ($parallax)? 'data-enllax-ratio = " ' . $parallax_data . ' "' :'';
$parallax_class = ($parallax)? 'parallax':'no_parallax';
//attribute
$class = get_sub_field('class');
$class = ($class) ? ' ' . $class : '';
$class = $class . ' ' . $parallax_class; 
//bg
$style_bg = get_sub_field('background');
//overlay
$background_overlay = get_sub_field('background_overlay');
$overlay_opacity = get_sub_field('overlay_opacity');
if ($style_bg == 'image'):
$overlayDiv = '<div class="parallax-overlay"></div>';
else :
$overlayDiv = '';
endif;
?>
	<?php if ($editor): ?>
		<section class="block-text block-flex <?php echo $class; ?>" id="flexible-section-<?php echo get_row_index(); ?>" <?php echo $parallax_data;?>>
			<?php echo $overlayDiv; ?>
			<div class="<?php echo esc_html($container); ?>">
				<div class="row">
					<div class="col-md-12">
							<?php the_sub_field('editor'); ?>
					</div>
				</div>
			</div>
		</section>
	<?php endif; ?>
<!--end block text-->
<?php endif; ?>