<?php if( get_row_layout() == 'block_carousels' ): ?>
<?php 
// vars
$my_id = get_sub_field('id');
$navigation = get_sub_field('navigation');
$pagination = get_sub_field('pagination');
$select_carousel = get_sub_field('select_carousel');
?>
<?php if ( $select_carousel) : ?>
<?php 
// override $post
$post = $select_carousel;
setup_postdata( $post ); 
// vars
$style_bg = get_field('background');
$background_image = get_field('background_image');
$background_overlay = get_field('background_overlay');
$background_overlay = ($background_overlay)? $background_overlay:'transparent';
$background_color = get_field('background_color');
$background_color = ($background_color)? $background_color:'transparent';
$opacity_overlay = get_field('opacity_overlay');
$color_text = get_field('color_text');
$color = ($color_text)? $color_text:'';
$image = get_field('image');
$parallax = get_field('enable_parallax');
$parallax_data = get_field('data_parallax');
$parallax_data = ($parallax_data)? 'data-enllax-ratio = " ' . $parallax_data . ' "' :'';
$parallax_class = ($parallax)? 'parallax':'no_parallax';
$paddingTop = get_field('padding_top');
$paddingBottom = get_field('padding_bottom');
$paddingTop  = (!empty($paddingTop) || ($paddingTop == '0')) ? 'padding-top:' . $paddingTop . 'px;' : $paddingTop = 'padding-top:80px;';
$paddingBottom  = (!empty($paddingBottom) || ($paddingBottom == '0')) ? 'padding-bottom:' .  $paddingBottom . 'px;' : $paddingBottom = 'padding-bottom:80px;';
?>
<section <?php if($my_id): ?>id="<?php echo esc_html($my_id); ?>"<?php endif; ?> class="block-carousel block-flex lazy <?php echo esc_html($parallax_class); ?>" <?php if ($style_bg == 'image') : ?>data-src="<?php echo esc_html($background_image['url']); ?>" <?php echo esc_html($parallax_data); ?><?php endif; ?> style="<?php echo esc_html($paddingTop); ?> <?php echo esc_html($paddingBottom); ?> background-color:<?php echo esc_html($background_color); ?>;">
	<?php if ($style_bg == 'image') : ?><div class="overlay" style="background-color:<?php echo esc_html($background_overlay); ?>; opacity:<?php echo esc_html($opacity_overlay); ?>;"></div><?php endif; ?>
	<div class="swiper-container swiper-container-carousel mb-0" id="block_carousel<?php echo get_row_index(); ?>">
		<div class="swiper-wrapper">
			
			<?php get_template_part('acf-components/loop/loop', 'carousel'); ?>
		</div><!--end swiper-wrapper-->
		<?php if ($pagination) : ?>
		<!-- Add Pagination -->
		<div class="swiper-pagination"></div>
		<?php endif; ?>
		<?php if ($navigation) : ?>
		<!-- Add Arrows -->
		<div class="swiper-button-next swiper-button-white"></div>
		<div class="swiper-button-prev swiper-button-white"></div>
		<?php endif; ?>
	</div><!--end swiper-container-->
</section>
<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
	<?php else: ?>
			<div class="alert alert-warning centered">
				<?php _e('Sorry, no results were found.', 'franky'); ?>
			</div>
			<?php  endif; ?>
<?php endif; ?>