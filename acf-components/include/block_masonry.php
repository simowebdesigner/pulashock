<?php if( get_row_layout() == 'block_masonry' ): ?>
<?php 
//include generic style
include(locate_template('acf-components/include/style_block.php')); 
//vars
$container = get_sub_field('container');
$container = 'container';
$style_class = 'container-portfolio';
$column = get_sub_field('columns_portfolio');
$post_type = get_sub_field('data_source'); 
$scroll = get_sub_field('scroll_ajax');
$scroll = ($scroll) ? 'true' : 'false';
$ajax = get_sub_field('ajax_load_more');
$show_filter = get_sub_field('show_filter');
$show_button = get_sub_field('show_button');
$show_button_class = ($show_button) ? $show_button_class = ' show-button' : $show_button_class = ' hide-button'; 
$order = get_sub_field('order_ajax');
$order_tax = get_sub_field('order_ajax_taxonomy');
$order_by = get_sub_field('order_by_ajax');
$order_by_tax = get_sub_field('order_by_taxonomy');
$posts_per_page = get_sub_field('posts_per_page_ajax');
$offset = get_sub_field('offset');
$loading_label = get_sub_field('loading_label_ajax');
$loading_label = ($loading_label) ? $loading_label : 'Load More';
$masonry_style = get_sub_field('masonry_style');
$masonry_gutters = get_sub_field('masonry_gutters');
$masonry_gutters = ($masonry_gutters) ? 'masonry-gutters' : 'no-gutters';
$image_featured = get_sub_field('image_featured');
$exclude_categories = get_sub_field('exclude_categories');
$tag__not_in = get_sub_field('exclude_tags');
$category = get_sub_field('category');
$progress_bar = get_sub_field('progress_bar');
$progress_bar_color = get_sub_field('progress_bar_color');
$progress_bar_color = trim($progress_bar_color,"#");
$data_source_cat = get_sub_field('data_source_cat');
if ($data_source_cat) {
	//$exclude_terms = $exclude_categories;	
	$terms = get_terms($data_source_cat, array(
		'post_type' => array($post_type),
		'fields' => 'all',
		'exclude' => $exclude_categories,
		'order' => $order_tax,
		'orderby' => $order_by_tax,

	));
	$data_taxonomy = $data_source_cat;
} else {
	$data_taxonomy = '';
}
//var passing to default
$overlay_opacity = get_sub_field('overlay_opacity');
$overlay_hover_opacity = get_sub_field('overlay_hover_opacity');
$_SESSION['overlay_opacity'] = $overlay_opacity;
$_SESSION['overlay_hover_opacity'] = $overlay_hover_opacity;
$_SESSION['image_featured'] = $image_featured;
$_SESSION['data_taxonomy'] = $data_taxonomy;
$overlay_item = get_sub_field('overlay_item');
if ($overlay_item == 'select_color') {
	$overlay_color_item = get_sub_field('overlay_color_item');
	$overlay_color_item = ($overlay_color_item) ? $overlay_color_item : '#000000';
	$_SESSION['overlay_color'] = $overlay_color_item;
} else {
	$_SESSION['overlay_color'] = '';
}
?>
<!--Portfolio-->
<section  class="content-portfolio block-flex<?php echo $class; ?>" <?php echo $id; ?>>
		<?php 
		if ($show_filter) {
			if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
				$count = count($terms);
				if ( $count > 0 ){
					echo '<div class="button-group filter-button-group alm-filter-nav">';
					echo '<button class="btn btn-link btn-readmore" data-post-type="'. $post_type .'" data-repeater="default" data-taxonomy="">'. __( "All", "start" ) . '</button>';
					foreach ( $terms as $term ) {
						$termname = strtolower($term->name);  
						$termname = str_replace(' ', '-', $termname);  
						echo '<button class="btn btn-link" data-post-type="'. $post_type .'" data-repeater="default" data-taxonomy="'. $data_taxonomy .'" data-taxonomy-terms="' . '.' . $termname . '">' . $term->name . '</button>'; //if ajax filter data-taxonomy-terms
					}
					echo '</div>';
				}
			}	
		}
		?>
	<div class="<?php echo esc_html($show_button_class); ?>">
		<div id="" class="container-isotope container-block-portfolio <?php echo 'column-' . $column; ?> <?php echo esc_html($masonry_style) . ' ' .  esc_html($masonry_gutters); ?>">
			<div class="grid-sizer"></div>
			<?php 
			$arrayAddLoadmore = array(
				"post_type" => $post_type,
				"posts_per_page" => $posts_per_page,
				"order" => $order,
				"order_by" => $order_by,
     "offset" => $offset,
				"scroll" => $scroll,
				"loading_label" => $loading_label,
				"category__not_in" => $exclude_categories,
     "category" => $category,
     "transition" => "masonry",
				"tag__not_in" => $exclude_categories,
				"progress_bar" => $progress_bar,
				"progress_bar_color" => $progress_bar_color,
			); 
			?>
			<?php echo franky_addLoadmore($arrayAddLoadmore); ?>
		</div>
	</div><!--end container-->
</section>
<!--End Portfolio-->
<?php endif; ?>