<?php if( get_row_layout() == 'block_gallery' ): ?>
<?php
/////////////////////////
// Gallery
////////////////////////
//vars
$navigation = get_sub_field('navigation');
$pagination = get_sub_field('pagination');
$background_image = get_sub_field('background_image');
$paddingTop = get_sub_field('padding_top');
$paddingBottom = get_sub_field('padding_bottom'); 
$paddingTop  = (!empty($paddingTop) || ($paddingTop == '0')) ? 'padding-top:' . $paddingTop . 'px;' : $paddingTop = 'padding-top:40px;';
$paddingBottom  = (!empty($paddingBottom) || ($paddingBottom == '0')) ? 'padding-bottom:' .  $paddingBottom . 'px;' : $paddingBottom = 'padding-bottom:40px;';
$container = get_sub_field('container');
$container = ($container) ? $container : 'container';
$style_bg = get_sub_field('background');
$id = get_sub_field('id');
?>
<section <?php if($id): ?>id="<?php echo esc_html($id); ?>"<?php endif; ?> class="block-gallery block-flex parallax lazy" <?php if ($style_bg == 'image') : ?>data-src="<?php echo esc_html($background_image['url']); ?>" <?php echo esc_html($parallax_data); ?> <?php endif; ?> style="
<?php echo esc_html($paddingTop); ?> <?php echo esc_html($paddingBottom); ?> background-color:<?php echo esc_html($background_color); ?>">
	<div class="parallax-overlay" <?php if ($style_bg == 'image') : ?>style="background-color:<?php echo esc_html($background_overlay); ?>; opacity:<?php echo esc_html($overlay_opacity); ?>"<?php endif; ?>></div>
	<?php 
$images = get_field('gallery_img');
if( $images ): ?>
	<div class="swiper-container block_carousel mb-0">
		<div class="swiper-wrapper">
			<?php foreach( $images as $image ): ?>
			<div class="swiper-slide">
				<div class="overlay"></div>
				<a href="<?php echo esc_html($image['url']); ?>">
					<img src="<?php echo esc_html($image['sizes']['thumbnail']); ?>" alt="<?php echo esc_html($image['alt']); ?>" />
				</a>
				<p><?php echo esc_html($image['caption']); ?></p>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
	<?php endif; ?>
	<?php if ($pagination) : ?>
	<!-- Add Pagination -->
	<div class="swiper-pagination"></div>
	<?php endif; ?>
	<?php if ($navigation) : ?>
	<!-- Add Arrows -->
	<div class="swiper-button-next swiper-button-white"></div>
	<div class="swiper-button-prev swiper-button-white"></div>
	<?php endif; ?>
</section>
<?php endif; ?>