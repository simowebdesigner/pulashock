<?php if( get_row_layout() == 'call_to_action' ): ?>
<?php 
$id = get_sub_field('id'); 
$select_call_to_action = get_sub_field('select_call_to_action');
?>
<?php if ($select_call_to_action) : ?>
<?php 
// override $post
$post = $select_call_to_action;
setup_postdata( $post );
// vars
$background_image = get_field('background_image');
$background_overlay = get_field('background_overlay');
$background_overlay = ($background_overlay)? $background_overlay:'transparent';
$video_url = get_field('video_url');
$youtube_url = 'https://youtube.com/embed/'. $video_url .'?&controls=0&showinfo=0&rel=0&autoplay=1&mute=1&loop=1&playlist='. $video_url;
$background_color = get_field('background_color');
$background_color = ($background_color)? $background_color:'transparent';
$opacity_overlay = get_field('opacity_overlay');
$color_text = get_field('color_text');
$color = ($color_text)? $color_text:'';
$image = get_field('image');
$parallax = get_field('enable_parallax');
$parallax_data = get_field('data_parallax');
$parallax_data = ($parallax_data)? 'data-enllax-ratio="'. $parallax_data .'"' :'';
$parallax_class = ($parallax)? 'parallax':'no_parallax';
$paddingTop = get_field('padding_top');
$paddingBottom = get_field('padding_bottom');
$paddingTop  = (!empty($paddingTop) || ($paddingTop == '0')) ? 'padding-top:' . $paddingTop . 'px;' : $paddingTop = 'padding-top:100px;';
$paddingBottom  = (!empty($paddingBottom) || ($paddingBottom == '0')) ? 'padding-bottom:' .  $paddingBottom . 'px;' : $paddingBottom = 'padding-bottom:100px;';
$id = get_sub_field('id');
$button_text = get_field('button_text');
$button_link = get_field('button_link');
$style_bg = get_field('background');
$align_button = get_field('align_button');
$rowN = get_row_index();	
?>
<!--call to action-->

<section <?php if($id): ?>id="<?php echo esc_html($id); ?>"<?php endif; ?> class="call-to-action block-flex lazy <?php echo esc_html($parallax_class); ?>" <?php if ($style_bg == 'image') : ?> data-src="<?php echo esc_html($background_image['url']); ?>"<?php endif; ?> <?php if ($parallax) : ?><?php echo esc_html($parallax_data); ?><?php endif; ?> style="<?php echo esc_html($paddingTop); ?> <?php echo esc_html($paddingBottom); ?> background-color:<?php echo esc_html($background_color); ?>;">

	<?php if ($style_bg == 'image' || $style_bg == 'video') : ?><div class="overlay" style="background-color:<?php echo esc_html($background_overlay); ?>; opacity:<?php echo esc_html($opacity_overlay); ?>;"></div><?php endif; ?>
	
	<?php  if ($style_bg == 'video') : ?>
	<?php  if ($video_url) : ?>
	<div class="container-video-bg fade-scroll" id="iframe_<?php echo esc_html($rowN); ?>" data-vp-add-class="fadeIn animated">
		<iframe class="iframe-video" src="<?php echo esc_html($youtube_url); ?>" frameborder="0"></iframe>
	</div>
	<?php endif; ?>
	<?php endif; ?>
	<div class="container">
		<div class="box-text fade-scroll" data-vp-add-class="fadeIn animated">
			<div><?php the_field('text_contact'); ?></div>
				<?php if( have_rows('buttons')): ?>
					<div class="readmore-content <?php echo esc_html($align_button); ?>" >
						<?php while ( have_rows('buttons') ) : the_row(); 
						//var 
						$button_link = get_sub_field('button_link');
						$button_text = get_sub_field('button_text');	
						$button_class = get_sub_field('button_class');
						$button_class	= ($button_class) ? $button_class : 'btn-readmore';
						?>
						<?php if ($button_text) : ?>
						<a href="<?php echo esc_html($button_link); ?>" class="btn <?php echo esc_html($button_class); ?>"><?php echo esc_html($button_text); ?></a>
						<?php endif; ?>
						<?php endwhile; ?>
					</div>
					<?php endif; ?>
		</div>
	</div><!--end container-->
</section>
<!--end call to action-->
<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php else: ?>
<div class="alert container alert-warning centered">
	<?php _e('Sorry, no results were found.', 'franky'); ?>
</div>
<?php  endif; ?>
<?php endif; ?>