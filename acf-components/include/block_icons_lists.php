<?php if( get_row_layout() == 'block_list_icons' ): ?>
<?php
//include generic style
include(locate_template('acf-components/include/style_block.php')); 
$rounded = get_sub_field('icons_rounded'); 
$rounded_icons = ($rounded)? $rounded:'icons_rounded'; 
$color_icons = get_sub_field('color_icons'); 
$color_icons = ($color_icons)? $color_icons:'#333';
$background_color_box = get_sub_field('background_color_box'); 
$background_color_box = ($background_color_box) ? $background_color_box :'transparent'; 
$background_icons = get_sub_field('background_icons'); 
$background_icons = ($background_icons) ? $background_icons :'transparent'; 
$title = get_sub_field('title');
$align_text = get_sub_field('align_text_side');
$enable_text_side = get_sub_field('enable_text_side');
$class_side = ($enable_text_side)? 'col-lg-6' : 'col-lg-12';
$text_side = get_sub_field('text');
$button = get_sub_field('button_link');
?>
<!--feature-icons-->
<section class="feature-icons block-icons-lists block-flex <?php echo esc_html($rounded_icons); ?> <?php echo $class; ?>" <?php echo $parallax_data;?>>
	<div class="container">
		<div class="row">
			<?php if($enable_text_side) : ?>
			<?php if($align_text == "left") : ?>
			<div class="col-lg-6">
			<div class="content-text">
					<?php if ($title) : ?>
				<div class="title-icons">
					<h2><?php echo esc_html($title); ?></h2>
				</div>
				<?php endif; ?>
				<?php the_sub_field('text'); ?>
				<?php if ($button): ?>
				<div class="clear">
					<a href="<?php echo esc_html($button['url']); ?>" target="<?php echo $button['target']; ?>" class="btn btn-readmore"><?php echo esc_html($button['title']); ?></a>
				</div>
				<?php endif; ?>
				</div>
			</div>
			<?php endif; ?>
			<?php endif; ?>
			<div class="<?php echo esc_html($class_side); ?>">
				<div class="block-columns-icons ">
					<?php $i = 0; while( have_rows('icons_feature_icons') ): the_row(); 
					// vars
					$icon = get_sub_field('icon');
					$title = get_sub_field('title');
					$text = get_sub_field('text');
					?>
					<div class="item-col <?php if (empty($icon)){?> empty-item <?php } ?> column-<?php echo esc_html($i); // count column ?> fade-scroll" data-vp-add-class="fadeIn animated">
						<div class="box-content">
							<?php if( get_sub_field('icon') ): ?>
							<div class="box-icon" style="background-color:<?php echo esc_html($background_icons); ?>; color:<?php echo esc_html($color_icons); ?>">
								<?php the_sub_field('icon'); ?>
							</div>
							<?php endif; ?>
							<div class="box-text">
								<?php if( get_sub_field('title') ): ?>
								<h5 class="title-icon"><?php echo esc_html($title); ?></h5>
								<?php endif; ?>
								<?php if( get_sub_field('text') ): ?>
								<?php the_sub_field('text'); ?>
								<?php endif; ?>
							</div>
						</div><!--end box-content-->
					</div><!--end item-col-->
					<?php endwhile; ?>
				</div><!--end block-columns-icons-->
			</div><!--end col-lg-->
			<?php if($enable_text_side) : ?>
			<?php if($align_text == "right") : ?>
			<div class="col-lg-6">
				<div class="content-text">
						<?php if ($title) : ?>
				<div class="title-icons">
					<h2><?php echo esc_html($title); ?></h2>
				</div>
				<?php endif; ?>
					<?php the_sub_field('text'); ?>
					<?php if ($button): ?>
					<div class="clear">
						<a href="<?php echo esc_html($button['url']); ?>" target="<?php echo $button['target']; ?>" class="btn btn-readmore"><?php echo esc_html($button['title']); ?></a>
					</div>
					<?php endif; ?>
				</div>
			</div>
			<?php endif; ?>
			<?php endif; ?>
		</div><!--end row-->
	</div><!--end container-->
</section><!--end-->
<!--end feauture icons-->
<?php endif; ?>