<?php if( get_row_layout() == 'block_map' ): ?>
<?php 
//vars
$paddingTop = get_sub_field('padding_top'); 
$paddingBottom = get_sub_field('padding_bottom'); 
$paddingTop  = (!empty($paddingTop) || ($paddingTop == '0')) ? 'padding-top:' . $paddingTop . 'px;' : $paddingTop = 'padding-top:40px;';
$paddingBottom  = (!empty($paddingBottom) || ($paddingBottom == '0')) ? 'padding-bottom:' .  $paddingBottom . 'px;' : $paddingBottom = 'padding-bottom:40px;';
$container = get_sub_field('container');
$id = get_sub_field('id'); ?>
<?php if ($container  == "container-fluid") : ?>
<?php	 $container = ''; ?>
<?php else : ?>
<?php	$container = ($container) ? $container : 'container' ?>
<?php endif; ?>
<!--Block contact-->
<?php if( have_rows('locations') ): ?>
<section <?php if($id): ?>id="<?php echo esc_html($id); ?>"<?php endif; ?> class="block-map block-flex" style="<?php echo esc_html($paddingTop); ?> <?php echo esc_html($paddingBottom); ?>">
	<div class="<?php echo esc_html($container); ?>"> 
		<div class="acf-map">
			<?php while ( have_rows('locations') ) : the_row(); 
			$location = get_sub_field('location');
			$link = get_sub_field('link');
			$title = get_sub_field('title');
			$description = get_sub_field('description');
			?>
			<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
				<?php if( $title ): ?>
				<h4><?php the_sub_field('title'); ?></h4>
				<?php endif; ?>	
				<p>
					<?php if( $description ): ?>
					<?php the_sub_field('description'); ?><br>
					<?php endif; ?>	
					<?php if( $link ): ?>
					<a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
					<?php endif; ?>	
				</p>
			</div>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>
<!--End Block map-->
<?php endif; ?>