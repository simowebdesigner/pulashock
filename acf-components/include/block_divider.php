<?php if( get_row_layout() == 'block_divider' ): ?>
<?php 
//vars
$background_color = get_sub_field('background_color');
$background_color = ($background_color)? $background_color:'transparent';
$paddingTop = get_sub_field('padding_top'); 
$paddingBottom = get_sub_field('padding_bottom');
$paddingTop  = (!empty($paddingTop) || ($paddingTop == '0')) ? 'padding-top:' . $paddingTop . ';px' : $paddingTop = 'padding-top:40px;';
$paddingBottom  = (!empty($paddingBottom) || ($paddingBottom == '0')) ? 'padding-bottom:' .  $paddingBottom . ';px' : $paddingBottom = 'padding-bottom:40px;';
 
$divider_icon = get_sub_field('divider_icon'); 
$id = get_sub_field('id');
?>
<section <?php if($id): ?>id="<?php echo esc_html($id); ?>"<?php endif; ?> class="block-divider block-flex" style="<?php echo esc_html($paddingTop); ?> <?php echo esc_html($paddingBottom); ?> background-color:<?php echo esc_html($background_color); ?>;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php if ($divider_icon) : ?>
				<div class="hr-custom fade-scroll" data-vp-add-class="fadeInUp animated">
				<span class="hr-inner"></span><span class="hr-icon"><?php the_sub_field('divider_icon');  ?></span><span class="hr-inner"></span>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>
