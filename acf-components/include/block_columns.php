
<?php if( get_row_layout() == 'block_columns' ): ?>
<?php 
$title = get_sub_field('title');
$text = get_sub_field('text');
$image = get_sub_field('image');
$container = get_sub_field('container');
$container = ($container) ? $container : 'container';
$count = 0;
//parallax
$parallax = get_sub_field('enable_parallax');
$parallax_data = get_sub_field('data_parallax');
$parallax_data = ($parallax)? 'data-enllax-ratio = " ' . $parallax_data . ' "' :'';
$parallax_class = ($parallax)? 'parallax':'no_parallax';
//attribute
$class = get_sub_field('class');
$class = ($class) ? ' ' . $class : '';
$class = $class . ' ' . $parallax_class; 
//bg
$style_bg = get_sub_field('background');
if ($style_bg == 'image'):
$overlayDiv = '<div class="parallax-overlay"></div>';
else :
$overlayDiv = '';
endif;
?>
<!--call to action-->
<section class="block-columns block-flex <?php echo $class; ?>" id="flexible-section-<?php echo get_row_index(); ?>">
	<?php echo $overlayDiv; ?>
	<div class="<?php echo esc_html($container); ?> container-parallax">
		<div class="row">
			<?php  while( have_rows('column') ): the_row();
			// vars
			$color = get_sub_field('color_text');
			$editor = get_sub_field('editor');
			$align_text= get_sub_field('align_text');
			$align_text = ($align_text) ? $align_text : 'left';
			$image = get_sub_field('image');
			$button_text = get_sub_field('button_text');
			$button_link = get_sub_field('button_link');
			$background_color = get_sub_field('background_color');
			$background_color = ($background_color)? 'background-color:' . $background_color . ';' : 'padding-left:0; padding-right:0;';
			$count ++;
			?>
			<div class="col column-<?php echo esc_html($count); // count column ?> text-<?php echo esc_html($align_text); ?>" style="color:<?php echo esc_html($color); ?>">
				<?php if( !empty($image) ): ?> 
				<div class="fade-scroll box-img" data-vp-add-class="fadeIn animated">
					<?php if ($button_link) : ?><a href="<?php echo esc_html($button_link); ?>"><?php endif; ?>
					<?php echo wp_get_attachment_image($image, 'large', 0, array('class' => 'img-responsive')); ?>
					<?php if ($button_link) : ?></a><?php endif; ?>
				</div>
				<?php endif; ?>
				<?php if ($editor): ?>
				<div class="fade-scroll box-text" data-vp-add-class="fadeIn animated" style="<?php echo esc_html($background_color); ?>;">
					<?php the_sub_field('editor'); ?>
				</div>
				<?php endif; ?>
				<?php if ($button_text): ?>
					<div class="clear fade-scroll box-text pt-0" data-vp-add-class="fadeIn animated" style="<?php echo esc_html($background_color); ?>;">
						<a href="<?php echo esc_html($button_link); ?>" class="btn btn-readmore mt-10"><?php echo esc_html($button_text); ?></a>
					</div>
					<?php endif; ?>
			</div><!--end item-col-->
			<?php endwhile; ?>
		</div><!--end row-->
	</div><!--end container-->
</section>
<!--end call to action-->
<?php endif; ?>