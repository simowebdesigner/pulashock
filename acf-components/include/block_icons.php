<?php if( get_row_layout() == 'block_icons' ): ?>
<?php 
//include generic style
include(locate_template('acf-components/include/style_block.php')); 
$style = get_sub_field('layout'); 
$style_icons = ($style)? $style:'style1';
$rounded = get_sub_field('icons_rounded'); 
$rounded_icons = ($rounded)? $rounded:''; 
$button_text = get_sub_field('button_text');
$button_link = get_sub_field('button_link');
$container = get_sub_field('container'); 
$style_bg = get_sub_field('background');
$columns = get_sub_field('columns_icons');
$columns = ($columns) ? $columns : '3';
if ($columns == 3) {
	$col_class = 'col-lg-4';
}
elseif ($columns == 4) {
	$col_class = 'col-lg-3';
}
?>
<?php if ($style == "style3") : ?>
<?php	 $container = 'container-full'; ?>
<?php $row = 'row row-no-margin'; ?>
<?php else : ?>
<?php	$container = ($container) ? $container : 'container' ?>
<?php $row = 'row'; ?>
<?php endif; ?>
<!--feature-icons-->
<section <?php echo $id;?> class="feature-icons block-columns-icons block-columns-three block-flex <?php echo esc_html($style_icons); ?> <?php echo esc_html($rounded_icons); ?> <?php echo $class; ?>" <?php $parallax_data;?>>
	<?php echo $overlayDiv; ?>
	<div class="<?php echo esc_html($container); ?> container-parallax">
		<?php if ($style == "style3") : ?>
		<?php $i = 0; ?>
		<?php while( have_rows('icons_feature_icons') ): the_row(); 
		// vars
		$i++;
		$icon = get_sub_field('icon');
		$title = get_sub_field('title');
		$text = get_sub_field('text'); ?>
		<?php if ($i == 1) : ?>
		<div class="<?php echo esc_html($row); ?>">
			<?php endif; ?>
			<div class="<?php echo esc_html($col_class); ?> column-<?php echo esc_html($i); // count column ?> fade-scroll" data-vp-add-class="fadeIn animated">
				<div class="box-content">
					<?php if( get_sub_field('icon') ): ?>
					<div class="box-icon">
						<?php the_sub_field('icon'); ?>
					</div>
					<?php endif; ?>
					<div class="box-text">
						<?php if($title): ?>
						<div class="title-icon"><?php echo esc_html($title); ?></div>
						<?php endif; ?>
						<?php if($text): ?>
						<?php the_sub_field('text'); ?>
						<?php endif; ?>
					</div>
				</div><!--end box-content-->
			</div><!--end col-->
			<?php  if($i % $columns == 0) : ?>
		</div><!--end row-->
		<div class="<?php echo esc_html($row); ?>">
			<?php endif; ?>
			<?php endwhile; ?>
		</div><!--end row-->
		<?php if ($button_text): ?>
		<div class="clear text-center">
			<a href="<?php echo esc_html($button_link); ?>" class="btn btn-readmore"><?php echo esc_html($button_text); ?></a>
		</div>
		<?php endif; ?>
		<?php else : //else style3  ?>
		<div class="block-columns-icons">
			<?php $i = 0; while( have_rows('icons_feature_icons') ): the_row(); 
			// vars
			$i++;
			$icon = get_sub_field('icon');
			$title = get_sub_field('title');
			$text = get_sub_field('text');
			?>
			<?php if ($i == 1) : ?>
			<div class="<?php echo esc_html($row); ?>">
				<?php endif; ?>
				<div class="<?php echo $col_class; ?> column-<?php echo $i; // count column ?> fade-scroll" data-vp-add-class="fadeIn animated">
					<div class="box-content">
						<?php if( get_sub_field('icon') ): ?>
						<div class="box-icon">
							<?php the_sub_field('icon'); ?>
						</div>
						<?php endif; ?>
						<div class="box-text">
							<?php if( get_sub_field('title') ): ?>
							<h5><?php echo esc_html($title); ?></h5>
							<?php endif; ?>
							<?php if( get_sub_field('text') ): ?>
							<?php the_sub_field('text'); ?>
							<?php endif; ?>
							<?php if ($button_text): ?>
							<div class="clear text-center">
								<a href="<?php echo esc_html($button_link); ?>" class="btn btn-readmore"><?php echo esc_html($button_text); ?></a>
							</div>
							<?php endif; ?>
						</div>
					</div><!--end box-content-->
				</div><!--end col-->
				<?php if($i % $columns == 0) : ?>
			</div><!--end row-->
			<div class="<?php echo esc_html($row); ?>">
				<?php endif; ?>
				<?php endwhile; ?>
			</div><!--end row-->
			</div><!--end block-columns-icons-->
			<?php endif; ?>
	</div><!--end container-->
</section><!--end feauture icons-->
<?php endif; ?>