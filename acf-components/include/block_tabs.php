<?php if( get_row_layout() == 'block_tabs' ): ?>
<!--block numbers-->
<?php 
//include generic style
include(locate_template('acf-components/include/style_block.php')); 
// vars
$align_text = get_sub_field('align_text_side');
$enable_text_side = get_sub_field('enable_text_side');
$class_width = ($enable_text_side)? 'col-lg-6' : 'col-lg-12';
$text_side = get_sub_field('text');
$style = get_sub_field('style');
$rowN = get_row_index();
?>
<section class="block-tabs block-flex <?php echo $class; ?>" <?php echo $id, $parallax_data;?>>
	<?php if ($style == 'default'): ?>
	<div class="default-tabs">
		<div class="container">
			<div class="row">
				<?php if($enable_text_side) : ?>
				<?php if($align_text == "left") : ?>
				<div class="col-lg-6">
					<?php the_sub_field('text'); ?>
				</div>
				<?php endif; ?>
				<?php endif; ?>
				<div class="<?php echo $class_width; ?>" id="content-nav<?php echo esc_html($rowN); ?>">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<?php $count = 0; ?>
						<?php while( have_rows('tabs') ): the_row(); 
					// vars
					$title = get_sub_field('title');
					$icon = get_sub_field('icon');
					$count++;
					if ($count == 1) {  
						$class_width = 'active';
					} 
					else { 
						$class_width = '';
					}
						?>
						<li class="nav-item"   >
							<a href="#tab<?php echo esc_html($count); ?>Content<?php echo esc_html($rowN); ?>" data-toggle="tab" role="tab" class="<?php echo $class_width; ?>">
								<?php the_sub_field('icon'); ?><?php echo esc_html($title); ?>
							</a>
						</li>
						<?php endwhile; ?>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<?php $i = 0; ?>
						<?php while( have_rows('tabs') ): the_row(); 
						// vars
						$text = get_sub_field('column_text');
						$button_text = get_sub_field('button_text');
						$button_link = get_sub_field('button_link');
						$i++;
						if ($i == 1) {  
							$class_width = 'active';
						} 
						else { 
							$class_width = '';
						}
						?>
						<div role="tabpanel" class="tab-pane <?php echo $class_width; ?>" id="tab<?php echo esc_html($i); ?>Content<?php echo esc_html($rowN); ?>">
							<?php if( have_rows('column_text') ): ?>
							<div class="row">
								<div class="block-columns">	
									<?php while( have_rows('column_text') ): the_row(); 
// vars
$text = get_sub_field('text'); ?>
									<div class="item-col">
										<?php the_sub_field('text'); ?>
									</div>
									<?php endwhile; ?>
								</div><!--end block-columns-->
							</div><!--end row-->
							<div class="clear"></div>
							<?php if ($button_text): ?>
							<div class="clear">
								<a href="<?php echo esc_html($button_link); ?>" class="btn btn-readmore-link"><?php echo esc_html($button_text); ?></a>
							</div>
							<?php endif; ?>
							<?php endif; ?>
						</div>
						<?php endwhile; ?>
					</div><!--end tab-content-->
				</div>
				<?php if($enable_text_side) : ?>
				<?php if($align_text == "right") : ?>
				<div class="col-lg-6">
					<?php the_sub_field('text'); ?>
				</div>
				<?php endif; ?>
				<?php endif; ?>
			</div>
		</div><!--end container-->
	</div><!--end default-tabs-->
	<?php elseif ($style == 'line') : ?>
	<div class="line-tabs">
		<!-- Nav tabs -->
		<div class="content-nav-line" id="content-nav<?php echo esc_html($rowN); ?>">
			<ul class="nav-tabs-line" role="tablist">
				<?php $count = 0; ?>
				<?php while( have_rows('tabs') ): the_row(); 
				// vars
				$title = get_sub_field('title');
				$icon = get_sub_field('icon');
				$count++;
				if ($count == 1) {  
					$class_width = 'active';
				} 
				else { 
					$class_width  = '';
				}
				?>
				<li class="nav-item">
					<a href="#tab<?php echo esc_html($count); ?>Content<?php echo esc_html($rowN); ?>" data-toggle="tab" role="tab" class="<?php echo $class_width; ?>">
						<?php the_sub_field('icon');?> <?php echo esc_html($title); ?>
					</a>
					<div class="border-after"></div>
				</li>
				<?php endwhile; ?>
			</ul>
		</div><!--end content-nav-line-->
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<!-- Tab panes -->
					<div class="tab-content">
						<?php $i = 0; ?>
						<?php while( have_rows('tabs') ): the_row(); 
					// vars
					$button_text = get_sub_field('button_text');
					$button_link = get_sub_field('button_link');
					$i++;
					if ($i == 1) {  
						$class_width = 'active';
					} 
					else { 
						$class_width = '';
					}
						?>
						<div role="tabpanel" class="tab-pane <?php echo $class_width; ?>" id="tab<?php echo esc_html($i); ?>Content<?php echo esc_html($rowN); ?>">
							<?php if( have_rows('column_text') ): ?>
							<div class="row">
								<div class="block-columns">	
									<?php while( have_rows('column_text') ): the_row(); 
// vars
$text = get_sub_field('text'); ?>
									<div class="item-col">
										<?php the_sub_field('text'); ?>
									</div>
									<?php endwhile; ?>
								</div><!--end block-columns-->
							</div><!--end row-->
							<div class="clear"></div>
							<?php if ($button_text): ?>
							<div class="clear">
								<a href="<?php echo esc_html($button_link); ?>" class="btn btn-readmore-link"><?php echo esc_html($button_text); ?></a>
							</div>
							<?php endif; ?>
							<?php endif; ?>
						</div>
						<?php endwhile; ?>
					</div><!--end tab-content-->
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>
</section>
<?php endif; ?>