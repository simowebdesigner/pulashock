<?php if( get_row_layout() == 'block_accordions' ): ?>
<?php 
//include generic style
include(locate_template('acf-components/include/style_block.php')); 
// vars
$align_text = get_sub_field('align_text_side');
$enable_text_side = get_sub_field('enable_text_side');
$class_side = ($enable_text_side)? 'col-lg-6' : 'col-lg-12';
$text_side = get_sub_field('text_side');
$rowN = get_row_index();
//parallax
$parallax = get_sub_field('enable_parallax');
$parallax_data = get_sub_field('data_parallax');
$parallax_data = ($parallax)? 'data-enllax-ratio = " ' . $parallax_data . ' "' :'';
$parallax_class = ($parallax)? 'parallax':'no_parallax';
//bg
$style_bg = get_sub_field('background');
//overlay
$background_overlay = get_sub_field('background_overlay');
$overlay_opacity = get_sub_field('overlay_opacity');
if ($style_bg == 'image'):
$overlayDiv = '<div class="parallax-overlay"></div>';
else :
$overlayDiv = '';
endif;
?>
<section  class="block-accordions block-flex <?php echo $class; ?>" <?phpecho $id; ?> <?php echo $parallax_data;?>>
<?php echo $overlayDiv; ?>
	<div class="container">
		<div class="row">
			<?php if($enable_text_side) : ?>
			<?php if($align_text == "left") : ?>
			<div class="col-lg-6">
				<?php the_sub_field('text_side'); ?>
			</div>
			<?php endif; ?>
			<?php endif; ?>
			<div class="<?php echo esc_html($class_side); ?>">
				<div class="panel-group" id="accordion<?php echo esc_html($rowN); ?>" role="tablist" aria-multiselectable="true">
					<?php $count = 0; ?>
					<?php while( have_rows('accordion') ): the_row(); 
					// vars
					$title = get_sub_field('title');
					$icon = get_sub_field('icon');
					$text = get_sub_field('text');
					$button_text = get_sub_field('button_text');
					$button_link = get_sub_field('button_link');
					$count++;
					if ($count == 1) {  
						$class_side = 'collapse show';
						$span_class = 'glyphicon-minus';
						$link_class = 'collapsed';
					} 
					else { 
						$class_side = 'collapse';
						$span_class = 'glyphicon-plus';
						$link_class = 'collapsed';
					}
					?>
					<div class="panel panel-default card">
						<div class="panel-heading card-header" role="tab" id="heading<?php echo esc_html($count); ?>accordion<?php echo esc_html($rowN); ?>">
							<h4 class="panel-title">
							<a class="panel-title" data-toggle="collapse" data-parent="#accordion<?php echo esc_html($rowN); ?>" href="#collapse<?php echo esc_html($count); ?>accordion<?php echo esc_html($rowN); ?>" aria-expanded="false" aria-controls="collapse<?php echo esc_html($count); ?>accordion<?php echo esc_html($rowN); ?>">
									<?php the_sub_field('icon'); ?> 
									<?php echo esc_html($title); ?>
									<span class="glyphicon <?php echo esc_html($span_class); ?> pull-right"></span>
							</a>
							</h4>
						</div>
						<div id="collapse<?php echo esc_html($count); ?>accordion<?php echo esc_html($rowN); ?>" class="<?php echo esc_html($class_side); ?>" role="tabpanel" aria-labelledby="heading<?php echo esc_html($count); ?>accordion<?php echo esc_html($rowN); ?>">
							<div class="panel-body">
								<?php the_sub_field('text'); ?>
								<div class="clear"></div>
								<?php if ($button_text): ?>
								<div class="clear">
									<a href="<?php echo esc_html($button_link); ?>" class="btn btn-readmore-link"><?php echo esc_html($button_text); ?></a>
								</div>
								<?php endif; ?>
							</div>
						</div>	
					</div><!--end panel-->					
					<?php endwhile; ?>
				</div><!--end panel-group-->
			</div>
			<?php if($enable_text_side) : ?>
			<?php if($align_text == "right") : ?>
			<div class="col-lg-6">
				<?php the_sub_field('text_side'); ?>
			</div>
			<?php endif; ?>
			<?php endif; ?>
		</div><!--end row-->
	</div><!--end container-->
</section>
<?php endif; ?>