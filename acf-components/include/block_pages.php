<?php if( get_row_layout() == 'block_pages' ): ?>
<?php 
//vars
$background_color = get_sub_field('background_color');
$background_color = ($background_color)? $background_color:'transparent';
$paddingTop = get_sub_field('padding_top'); 
$paddingBottom = get_sub_field('padding_bottom'); 
$paddingTop  = (!empty($paddingTop) || ($paddingTop == '0')) ? 'padding-top:' . $paddingTop . 'px;' : $paddingTop = 'padding-top:40px;';
$paddingBottom  = (!empty($paddingBottom) || ($paddingBottom == '0')) ? 'padding-bottom:' .  $paddingBottom . 'px;' : $paddingBottom = 'padding-bottom:40px;';
$id = get_sub_field('id');
$navigation = get_sub_field('navigation');
$pagination = get_sub_field('pagination');
$post_objects = get_sub_field('pages');
$layout_post = get_sub_field('layout_post');
$layout_post = ($layout_post) ? $layout_post : '3_column'; ?>
<!--Portfolio-->
<section <?php if($id): ?>id="<?php echo esc_html($id); ?>"<?php endif; ?> class="block-pages block-flex" style="<?php echo esc_html($paddingTop); ?> <?php echo esc_html($paddingBottom); ?> background-color:<?php echo esc_html($background_color); ?>;">
	<?php
	/*
*  Loop through post objects (assuming this is a multi-select field) ( setup postdata )
*  Using this method, you can use all the normal WP functions as the $post object is temporarily initialized within the loop
*  Read more: http://codex.wordpress.org/Template_Tags/get_posts#Reset_after_Postlists_with_offset
*/
	if( $post_objects ): ?>
	<div class="container-fluid">
		<?php if ($layout_post != 'alternate_image') : ?>
		<div class="row">
			<?php $i=0; foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
			<?php setup_postdata($post); ?>
			<?php $primary_color = get_field('primary_color');?>
			<?php  $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large'); ?>
			<div class="<?php echo $layout_post;?> fade-scroll content-bg-page" data-vp-add-class="fadeIn animated">
				<div class="box-img" style="background-image: url('<?php echo esc_url($large_image_url[0]) ?>');">
					<div class="overlay" <?php if ($primary_color) : ?>style="background-color:<?php echo esc_html($primary_color); ?>" <?php endif; ?>></div>
					<article <?php post_class(); ?>>
						<div class="content-box-page">
							<div class="box-text">
								<header>
									<a href="<?php the_permalink(); ?>">
										<h2 class="entry-title"><?php the_title(); ?></h2>
									</a>
									<?php get_template_part('templates/entry-meta'); ?>
								</header>
								<div class="entry-summary">
									<?php the_excerpt(); ?>
									<?php echo '<a href="'.get_permalink().'" class="btn btn-readmore-link mt-10">'.__('Read more', 'franky').'</a>'; ?>
								</div>
							</div>
						</div>
					</article>
				</div>
			</div><!--end col-md-4-->
			<?php endforeach; ?>
			<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
		</div>

		<?php elseif ($layout_post == 'alternate_image') : ?>

		<?php $i=1; foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
		<?php setup_postdata($post); ?>
		<article <?php post_class(); ?>>
			<div class="content-post-alternate row <?php if ($i % 2 == 0) { ?> even-post <?php } ?>" data-vp-add-class="fadeIn animated">
				<?php if ( has_post_thumbnail()): ?> 
				<figure class="col-lg-6">
					<a class="link-post" href="<?php the_permalink(); ?>">
						<div class="overlay"></div>
						<?php echo get_the_post_thumbnail($post->ID, 'news-thumb', array('class' => 'img-responsive w_full')); ?>
					</a>	
				</figure>
				<?php endif; ?>
				<div class="box-text <?php if ( has_post_thumbnail()): ?>col-lg-6  <?php else: ?>col-sm-12<?php endif; ?>">
					<header>
						<a href="<?php the_permalink(); ?>"><h3 class="entry-title"><?php the_title(); ?></h3></a>
						<?php get_template_part('templates/entry-meta'); ?>
					</header>
					<div class="entry-summary">
						<?php the_excerpt(); ?>
					</div>
					<?php echo '<a href="'.get_permalink().'" class="btn btn-readmore mt-10">'.__('Read more', 'franky').'</a>'; ?>
				</div>
			</div>
		</article>
		<?php $i++; ?>
		<?php endforeach; ?>
		<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
		<?php endif; ?>

	</div><!--end container-->
	<?php endif; ?>
</section>
<?php endif; ?>