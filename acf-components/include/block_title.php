<?php if( get_row_layout() == 'block_title' ): ?>
<?php 
//include generic style
include(locate_template('acf-components/include/style_block.php')); 
$button_text = get_sub_field('button_text');
$button = get_sub_field('button_link');
$title_block = get_sub_field('title_block');
$tag = get_sub_field('tag');
$tag = ($tag) ? $tag : 'h1';
$small_title = get_sub_field('small_title');
$description_block = get_sub_field('description_block');
$divider_icon = get_sub_field('divider_icon'); 
$show_divider = get_sub_field('show_divider'); 
$align = get_sub_field('align');

//if icon style
if ($show_divider == 'line') {
	if ($align == 'left') {
		$text_align = 'text-left';
		}
	elseif ($align == 'center') {
		$text_align = 'text-center';
	}
	elseif ($align == 'right') {
		$text_align = 'text-right';
	}
}
elseif ($show_divider == 'icon') {
	$text_align = 'text-center';
}
?>
<section class="block-title block-flex <?php echo $class . ' ' . $text_align; ?>" <?php echo $id, $data_anchor;?>>
<?php echo $overlayDiv; ?>
	<!--Block title-->
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<?php if ($small_title): ?>
				<div class="small-title fade-scroll" data-vp-add-class="fadeInUp animated">
					<?php echo esc_html($small_title); ?>
				</div>
				<?php endif; ?>
				<?php if ($show_divider == 'icon') : ?>
				<?php if ($title_block): ?>
				<<?php echo esc_html($tag); ?> class="title-big text-center fade-scroll" data-vp-add-class="fadeInUp animated"><?php the_sub_field('title_block'); ?></<?php echo esc_html($tag); ?>>
				<?php endif; ?>
				<?php if ($show_divider == 'icon') : ?>
				<?php if ($divider_icon) : ?>
				<div class="hr-custom fade-scroll" data-vp-add-class="fadeInUp animated">
					<span class="hr-inner"></span><span class="hr-icon"><?php the_sub_field('divider_icon');  ?></span><span class="hr-inner"></span>
				</div>
				<?php endif; ?>
				<?php endif; ?>
				<?php elseif ($show_divider == 'line') : ?>
				<?php if ($title_block): ?>
				<<?php echo esc_html($tag); ?> class="title-divider-line <?php echo esc_html($text_align); ?> fade-scroll" data-vp-add-class="fadeInUp animated"><?php the_sub_field('title_block'); ?></<?php echo esc_html($tag); ?>>
				<?php endif; ?>
				<?php endif; ?>
				<?php if ($description_block): ?>
				<div class="block-description fade-scroll" data-vp-add-class="fadeInUp animated">
					<?php the_sub_field('description_block'); ?>
				</div>
				<?php endif; ?>
				<?php if ($button ): ?>
				<div class="fade-scroll" data-vp-add-class="fadeInUp animated">
					<a href="<?php echo esc_html($button['url']); ?>" target="<?php echo $button['target']; ?>" class="btn btn-readmore"><?php echo esc_html($button['title']); ?></a>
				</div>
				<?php endif; ?>
			</div>
			<!--end col-sm-12-->
		</div>
		<!--end row-->
	</div><!--end container-->
	<!--end Block title-->
</section>
<?php endif; ?>