<?php 
//data-anchor
$data_anchor = get_sub_field('data_anchor');
$data_anchor = ' data-anchor="'.$data_anchor.'"';
//id
$id = 'id="flexible-section-'. get_row_index() .'"';
//parallax
$parallax = get_sub_field('enable_parallax');
$parallax_data = get_sub_field('data_parallax');
$parallax_data = ($parallax)? 'data-enllax-ratio = "' . $parallax_data . '"' :'';
$parallax_class = ($parallax)? 'parallax':'no_parallax';
//attribute
$class = get_sub_field('class');
$class = ($class) ?  $class : '';
$class = $class . ' ' . $parallax_class; 
//bg
$style_bg = get_sub_field('background');
//overlay
$background_overlay = get_sub_field('background_overlay');
$overlay_opacity = get_sub_field('overlay_opacity');
if ($style_bg == 'image'):
$overlayDiv = '<div class="parallax-overlay"></div>';
else :
$overlayDiv = '';
endif;
?>