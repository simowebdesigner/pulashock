
<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'franky'); ?>
  </div>
<?php endif; ?>
   <iframe src="https://rcm-eu.amazon-adsystem.com/e/cm?o=29&p=48&l=ur1&category=it_music&banner=0KNKKM01QFKP6N02Q202&f=ifr&linkID=7263ebaac2ea676a73864098a07be6ce&t=pulashock-21&tracking_id=pulashock-21" width="100%" height="90" scrolling="no" border="0" marginwidth="0" style="border:none;" frameborder="0"></iframe>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
<?php endwhile; ?>

<?php // the_posts_navigation(); ?>
<div class="col-sm-12 text-center">
	<?php the_posts_pagination( array(
	'mid_size' => 2,
	'prev_text' => __( '<i class=\'fa fa-long-arrow-left\' aria-hidden="true"> </i>', 'franky' ),
	'next_text' => __( '<i class=\'fa fa-long-arrow-right\' aria-hidden="true"> </i>', 'franky' ),
) ); ?>
</div>
