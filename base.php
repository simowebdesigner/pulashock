<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;
?>
<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
  <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9Z7Q7S"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
   <div class="no-animsition">
     <?php //include humburger-menu
      get_template_part('templates/humburger-menu');
      ?>
     <div class="main-wrapper baseee">
      <?php
        do_action('get_header');
        get_template_part('templates/header');
      ?>   
     <?php  get_template_part('templates/page', 'header'); ?>
     <div class="wrap container" role="document">
      <div class="content row">
       <main class="main col-md-9" id="container">
        <?php  include Wrapper\template_path(); ?>
       </main><!-- /.main -->
       <?php if (Setup\display_sidebar()) : ?>
        <div class="col-md-3">
        <aside class="sidebar">
         <?php include Wrapper\sidebar_path(); ?>
        </aside><!-- /.sidebar -->
        </div>
       <?php endif; ?>
      </div><!-- /.content -->

     </div><!-- /.wrap -->
     <?php get_template_part('templates/related', 'posts'); ?>
     <?php comments_template('/templates/comments.php'); ?>
      <?php
        do_action('get_footer');
        get_template_part('templates/footer');
        wp_footer();
        get_template_part('acf-components/custom-scripts');		
      ?>
    </div>
		</div>
  </body>
</html>